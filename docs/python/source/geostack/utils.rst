geostack.utils package
======================

Submodules
----------

geostack.utils.get\_projection module
-------------------------------------

.. automodule:: geostack.utils.get_projection
   :members: get_epsg
   :undoc-members:
   :show-inheritance:
   :noindex:

Module contents
---------------

.. automodule:: geostack.utils
   :members: percentile, quantile, is_ipython
   :undoc-members:
   :show-inheritance:
