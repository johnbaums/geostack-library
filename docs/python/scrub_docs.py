import os
import os.path as pth
import shutil

def remove_test_pth(filein):
    with open(pth.join(pth.dirname(filein), "test.rst"), "w") as out:
        with open(filein, 'r') as inp:
            for row in inp:
                if 'test' not in row:
                    out.write(row)
    shutil.move(pth.join(pth.dirname(filein), "test.rst"), filein)

source_dir = pth.join(os.getcwd(), "source", "geostack")

for file in os.listdir(source_dir):
    if "test" not in file:
        if file in ["geostack.rst", "modules.rst"]:
            os.remove(pth.join(source_dir, file))
        else:
            new_name=pth.join(source_dir, os.path.basename(file).replace("geostack.", ""))
            shutil.move(pth.join(source_dir, file), new_name)
            remove_test_pth(new_name)
    else:
        os.remove(pth.join(source_dir, file))
