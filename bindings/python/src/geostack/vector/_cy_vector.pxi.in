"""
Template for each `dtype` helper function

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# x_name, x_type

rtypes = [('d', 'double'),
         ('f', 'float'),]

ptypes = [('flt', 'float'),
          ('dbl', 'double'),
          ('int', 'int'),
          ("str", "string"),]

}}

{{for x_name, x_type in rtypes}}

cdef class _BoundingBox_{{x_name}}:
    def __cinit__(self, _Coordinate_{{x_name}} ll=None, _Coordinate_{{x_name}} ur=None):
        if ur is not None and ll is not None:
            self.thisptr.reset(new _boundingBox_{{x_name}}(deref(ll.thisptr),
                                                           deref(ur.thisptr)))
        else:
            self.thisptr.reset(new _boundingBox_{{x_name}}())

    cdef void c_copy(self, _boundingBox_{{x_name}} other):
        self.thisptr.reset()
        self.thisptr = move(make_shared[_boundingBox_{{x_name}}](other))

    cpdef {{x_type}} area2D(self):
        cdef {{x_type}} out
        out = deref(self.thisptr).area2D()
        return out

    cpdef {{x_type}} minimumDistanceSqr(self, _BoundingBox_{{x_name}} other):
        cdef {{x_type}} out
        out = deref(self.thisptr).minimumDistanceSqr(deref(other.thisptr))
        return out

    cpdef {{x_type}} centroidDistanceSqr(self, _BoundingBox_{{x_name}} other):
        cdef {{x_type}} out
        out = deref(self.thisptr).centroidDistanceSqr(deref(other.thisptr))
        return out

    cpdef _BoundingBox_{{x_name}} convert(self, _ProjectionParameters_d this,
                                          _ProjectionParameters_d other):
        out = _BoundingBox_{{x_name}}()
        out.c_copy(deref(self.thisptr).convert(deref(this.thisptr),
                                               deref(other.thisptr)))
        return out

    def to_list(self):
        out = []
        minCoordinate = _Coordinate_{{x_name}}(0.0, 0.0)
        minCoordinate.c_copy(deref(self.thisptr).min_c)

        maxCoordinate = _Coordinate_{{x_name}}(0.0, 0.0)
        maxCoordinate.c_copy(deref(self.thisptr).max_c)

        out.append(minCoordinate.to_list())
        out.append(maxCoordinate.to_list())
        return out

    @staticmethod
    def from_list(other):
        if len(other[0]) != 4 and len(other[1]) != 4:
            raise TypeError("input list should be length 4")
        ll = _Coordinate_{{x_name}}(<{{x_type}}> other[0][0],
                                   <{{x_type}}> other[0][1],
                                   r=<{{x_type}}> other[0][2],
                                   s=<{{x_type}}> other[0][3])
        ur = _Coordinate_{{x_name}}(<{{x_type}}> other[1][0],
                                    <{{x_type}}> other[1][1],
                                    r=<{{x_type}}> other[1][2],
                                    s=<{{x_type}}> other[1][3])
        out = _BoundingBox_{{x_name}}(ll=ll, ur=ur)
        return out

    cpdef void extend_with_value(self, {{x_type}} other):
        deref(self.thisptr).extend(other)
        self.c_copy(deref(self.thisptr))

    cpdef void extend_with_coordinate(self, _Coordinate_{{x_name}} other):
        deref(self.thisptr).extend(deref(other.thisptr))
        self.c_copy(deref(self.thisptr))

    cpdef void extend_with_bbox(self, _BoundingBox_{{x_name}} other):
        deref(self.thisptr).extend(deref(other.thisptr))
        self.c_copy(deref(self.thisptr))

    def extend(self, other):
        if isinstance(other, _Coordinate_{{x_name}}):
            self.extend_with_coordinate(other)
        elif isinstance(other, (int, float)):
            self.extend_with_value(<{{x_type}}>other)
        elif isinstance(other, _BoundingBox_{{x_name}}):
            self.extend_with_bbox(other)
        else:
            raise TypeError("datatype of input argument not recognized")

    cpdef _Coordinate_{{x_name}} centroid(self):
        _out = _Coordinate_{{x_name}}(0.0, 0.0)
        _out.c_copy(deref(self.thisptr).centroid())
        return _out

    cpdef _Coordinate_{{x_name}} extent(self):
        _out = _Coordinate_{{x_name}}(0.0, 0.0)
        _out.c_copy(deref(self.thisptr).extent())
        return _out

    cpdef void reset(self):
        deref(self.thisptr).reset()

    cpdef bool contains(self, _Coordinate_{{x_name}} c):
        cdef bool out
        out = deref(self.thisptr).contains(deref(c.thisptr))
        return out

    @staticmethod
    cdef bool _bbox_contains_coordinate(BoundingBox[{{x_type}}] A, Coordinate[{{x_type}}] c):
        cdef bool out
        out = BoundingBox[{{x_type}}].bbox_contains_coordinate(A, c)
        return out

    @staticmethod
    def bbox_contains_coordinate(_BoundingBox_{{x_name}} this, _Coordinate_{{x_name}} other):
        cdef bool out
        out = _BoundingBox_{{x_name}}._bbox_contains_coordinate(deref(this.thisptr),
            deref(other.thisptr))
        return out

    @staticmethod
    cdef bool _bbox_contains_bbox(BoundingBox[{{x_type}}] A, BoundingBox[{{x_type}}] B):
        cdef bool out
        out = BoundingBox[{{x_type}}].bbox_contains_bbox(A, B)
        return out

    @staticmethod
    def bbox_contains_bbox(_BoundingBox_{{x_name}} this, _BoundingBox_{{x_name}} other):
        cdef bool out
        out = _BoundingBox_{{x_name}}._bbox_contains_bbox(deref(this.thisptr),
            deref(other.thisptr))
        return out

    @staticmethod
    cdef bool _boundingBoxIntersects(BoundingBox[{{x_type}}] A, BoundingBox[{{x_type}}] B):
        cdef bool out
        out = BoundingBox[{{x_type}}].boundingBoxIntersects(A, B)
        return out

    @staticmethod
    def boundingBoxIntersects(_BoundingBox_{{x_name}} this, _BoundingBox_{{x_name}} other):
        cdef bool out
        out = _BoundingBox_{{x_name}}._boundingBoxIntersects(deref(this.thisptr),
            deref(other.thisptr))
        return out

    @property
    def min_c(self):
        out = _Coordinate_{{x_name}}(0.0, 0.0)
        out.c_copy(deref(self.thisptr).min_c)
        return out

    @property
    def max_c(self):
        out = _Coordinate_{{x_name}}(0.0, 0.0)
        out.c_copy(deref(self.thisptr).max_c)
        return out

    def __getitem__(self, idx):
        if idx > 1:
            raise IndexError("Only index [0,1] are valid")
        if idx == 0:
            minCoordinate = _Coordinate_{{x_name}}(0.0, 0.0)
            minCoordinate.c_copy(deref(self.thisptr).min_c)
            return minCoordinate
        else:
            maxCoordinate = _Coordinate_{{x_name}}(0.0, 0.0)
            maxCoordinate.c_copy(deref(self.thisptr).max_c)
            return maxCoordinate

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if type(self) is _BoundingBox_{{x_name}}:
            self.thisptr.reset()

cdef class _Coordinate_{{x_name}}:
    def __cinit__(self, {{x_type}} p, {{x_type}} q,
                  {{x_type}} r=0.0, {{x_type}} s=0.0):
        self.thisptr = new Coordinate[{{x_type}}](p, q, r, s)

    cdef void c_copy(self, _coordinate_{{x_name}} c):
        if self.thisptr is not NULL:
            del self.thisptr
        self.thisptr = new Coordinate[{{x_type}}](c)

    cpdef _Coordinate_{{x_name}} maxCoordinate(self, _Coordinate_{{x_name}} this,
                                               _Coordinate_{{x_name}} other):
        if self.thisptr is not NULL:
            out = _Coordinate_{{x_name}}()
            out.c_copy(deref(self.thisptr).max_c(deref(this.thisptr),
                                                 deref(other.thisptr)))
            return out

    cpdef _Coordinate_{{x_name}} minCoordinate(self, _Coordinate_{{x_name}} this,
                                               _Coordinate_{{x_name}} other):
        if self.thisptr is not NULL:
            out = _Coordinate_{{x_name}}()
            out.c_copy(deref(self.thisptr).min_c(deref(this.thisptr),
                                                 deref(other.thisptr)))
            return out

    cpdef _Coordinate_{{x_name}} centroid(self, _Coordinate_{{x_name}} this,
                                          _Coordinate_{{x_name}} other):
        if self.thisptr is not NULL:
            out = _Coordinate_{{x_name}}()
            out.c_copy(deref(self.thisptr).centroid(deref(this.thisptr),
                                                    deref(other.thisptr)))
            return out

    cpdef {{x_type}} magnitudeSquared(self):
        if self.thisptr is not NULL:
            return deref(self.thisptr).magnitudeSquared()

    cpdef string getGeoHash(self):
        cdef string out
        out = deref(self.thisptr).getGeoHash()
        return out

    def getCoordinate(self):
        if self.thisptr is not NULL:
            return (deref(self.thisptr).p, deref(self.thisptr).q,
                    deref(self.thisptr).r, deref(self.thisptr).s)
        else:
            raise AttributeError("class is not initialized")

    cpdef set_q(self, {{x_type}} other):
        if self.thisptr is not NULL:
            deref(self.thisptr).q = other
        else:
            raise AttributeError("class is not initialized")

    cpdef set_p(self, {{x_type}} other):
        if self.thisptr is not NULL:
            deref(self.thisptr).p = other
        else:
            raise AttributeError("class is not initialized")

    cpdef set_r(self, {{x_type}} other):
        if self.thisptr is not NULL:
            deref(self.thisptr).r = other
        else:
            raise AttributeError("class is not initialized")

    cpdef set_s(self, {{x_type}} other):
        if self.thisptr is not NULL:
            deref(self.thisptr).s = other
        else:
            raise AttributeError("class is not initialized")

    cpdef {{x_type}} get_q(self):
        if self.thisptr is not NULL:
            return deref(self.thisptr).q
        else:
            raise AttributeError("class is not initialized")

    cpdef {{x_type}} get_p(self):
        if self.thisptr is not NULL:
            return deref(self.thisptr).p
        else:
            raise AttributeError("class is not initialized")

    cpdef {{x_type}} get_r(self):
        if self.thisptr is not NULL:
            return deref(self.thisptr).r
        else:
            raise AttributeError("class is not initialized")

    cpdef {{x_type}} get_s(self):
        if self.thisptr is not NULL:
            return deref(self.thisptr).s
        else:
            raise AttributeError("class is not initialized")

    @staticmethod
    def from_list(other):
        if not isinstance(other, list):
            raise TypeError("Input argument should be a list")
        if len(other) != 4:
            raise ValueError("Input list should be of length 2")
        out = _Coordinate_{{x_name}}(<{{x_type}}> other[0], <{{x_type}}> other[1],
                                     r=<{{x_type}}> other[2], s=<{{x_type}}> other[3])
        return out

    def to_list(self):
        if self.thisptr is not NULL:
            return [self.get_p(), self.get_q(), self.get_r(), self.get_s()]
        else:
            raise AttributeError("class is not initialized")

    def __getitem__(self, idx):
        map_method = {3:self.get_s(), 2:self.get_r(),
                      1:self.get_q(), 0:self.get_p()}
        return map_method[idx]

    def __add__(_Coordinate_{{x_name}} self, _Coordinate_{{x_name}} other):
        out = _Coordinate_{{x_name}}(0.0, 0.0)
        out.thisptr[0] = <Coordinate[{{x_type}}]>self.thisptr[0] + <Coordinate[{{x_type}}]>other.thisptr[0]
        return out

    def __sub__(_Coordinate_{{x_name}} self, _Coordinate_{{x_name}} other):
        out = _Coordinate_{{x_name}}(0.0, 0.0)
        out.thisptr[0] = <Coordinate[{{x_type}}]>self.thisptr[0] - <Coordinate[{{x_type}}]>other.thisptr[0]
        return out

    def __eq__(self, _Coordinate_{{x_name}} other):
        cdef bool out = False
        out = deref(self.thisptr) == deref(other.thisptr)
        return out

    def __ne__(self, _Coordinate_{{x_name}} other):
        cdef bool out = True
        out = deref(self.thisptr) != deref(other.thisptr)
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if type(self) is _Coordinate_{{x_name}} and self.thisptr is not NULL:
            del self.thisptr

cdef class _Vector_{{x_name}}:
    def __cinit__(self):
        self.thisptr.reset(new Vector[{{x_type}}]())

    cdef void c_copy(self, Vector[{{x_type}}] v):
        self.thisptr.reset()
        self.thisptr = move(make_shared[Vector[{{x_type}}]](v))

    cdef size_t _add_point(self, Coordinate[{{x_type}}] c_) except+:
        cdef size_t ret = 0
        ret = deref(self.thisptr).addPoint(c_)
        return ret

    def addPoint(self, _Coordinate_{{x_name}} c):
        cdef size_t out
        out = self._add_point(deref(c.thisptr))
        return <int> out

    def addPoints(self, {{x_type}}[:, :] c):
        cdef size_t nx, i
        cdef _Coordinate_{{x_name}} cs
        cdef int[:] out
        nx = <size_t> c.shape[0]
        out = np.zeros(shape=(nx,), dtype=np.int32)
        for i in range(nx):
            cs = _Coordinate_{{x_name}}(c[i, 0],
                                        c[i, 1],
                                        r=c[i, 2],
                                        s=c[i, 3])
            out[i] = self._add_point(deref(cs.thisptr))
        return out

    cdef size_t _add_line_string(self, vector[Coordinate[{{x_type}}]] cs_) except+:
        cdef size_t ret = 0
        ret = deref(self.thisptr).addLineString(cs_)
        return ret

    def addLineString(self, {{x_type}}[:, :] cs_):
        cdef int i, nx
        cdef vector[Coordinate[{{x_type}}]] _list_of_coords
        cdef size_t out
        nx = <int> cs_.shape[0]

        if cs_.shape[1] != 4:
            raise TypeError("numpy array should be of dimensions (npoints, 4)")
        for i in range(nx):
            c_ = _Coordinate_{{x_name}}(cs_[i, 0],
                                        cs_[i, 1],
                                        r=cs_[i, 2],
                                        s=cs_[i, 3])
            _list_of_coords.push_back(deref(c_.thisptr))
        out = self._add_line_string(_list_of_coords)
        _list_of_coords.clear()
        return out

    cdef size_t _add_polygon(self, vector[vector[Coordinate[{{x_type}}]]] pcs_) except+:
        cdef size_t ret = 0
        ret = deref(self.thisptr).addPolygon(pcs_)
        return ret

    def addPolygon(self, list pcs):
        cdef int i, j, npolys, ncoords
        cdef vector[vector[Coordinate[{{x_type}}]]] pcs_
        cdef vector[Coordinate[{{x_type}}]] cs_
        cdef size_t out
        npolys = <int> len(pcs)
        for j in range(npolys):
            if not isinstance(pcs[j], np.ndarray):
                raise TypeError("only numpy arrays are acceptable")
            if <int> pcs[j].shape[1] != 4:
                raise ValueError("numpy array of coordinates should be (npoints,4)")
            ncoords = <int> pcs[j].shape[0]
            for i in range(ncoords):
                c_ = _Coordinate_{{x_name}}(<{{x_type}}> pcs[j][i, 0],
                                            <{{x_type}}> pcs[j][i, 1],
                                            r=<{{x_type}}> pcs[j][i, 2],
                                            s=<{{x_type}}> pcs[j][i, 3])
                cs_.push_back(deref(c_.thisptr))
            pcs_.push_back(cs_)
            cs_.clear()
        out = self._add_polygon(pcs_)
        pcs_.clear()
        return <int> out

#    cpdef cl_uint addGeometry(self, _Vector_{{x_name}} v, cl_uint idx) except+:
#        cdef cl_uint out
#        cdef shared_ptr[VectorGeometry[{{x_type}}]] vec_geom
#        if self.hasData():
#            vec_geom = v.get_geometry(idx)
#            out = deref(self.thisptr).addGeometry(shared_ptr[VectorGeometry[{{x_type}}]](deref(vec_geom).clone()))
#            return out
#        else:
#            raise RuntimeError("Vector object has not been initialised")

    cdef shared_ptr[VectorGeometry[{{x_type}}]] get_geometry(self, cl_uint idx) except+:
        cdef shared_ptr[VectorGeometry[{{x_type}}]] out
        with nogil:
            out = deref(self.thisptr).getGeometry(idx)
        return out

    cpdef size_t get_geometry_type(self, cl_uint idx) except+:
        cdef size_t geom
        cdef shared_ptr[VectorGeometry[{{x_type}}]] vec_geom
        vec_geom = self.get_geometry(idx)
        for geom in [GeometryType.NoType, GeometryType.Point,
                     GeometryType.LineString, GeometryType.Polygon,
                     GeometryType.TileType]:
            if deref(vec_geom).isType(geom):
                return geom

    cpdef void updatePointIndex(self, size_t index) except+:
        deref(self.thisptr).updatePointIndex(index)

    cpdef void updateLineStringIndex(self, size_t index) except+:
        deref(self.thisptr).updateLineStringIndex(index)

    cpdef void updatePolygonIndex(self, size_t index) except+:
        deref(self.thisptr).updatePolygonIndex(index)

    cpdef void clear(self) except+:
        deref(self.thisptr).clear()

    cpdef void buildTree(self) except+:
        deref(self.thisptr).buildTree()

    cpdef IndexList getGeometryIndexes(self) except+:
        cdef _index_list _geom_indices
        with nogil:
            _geom_indices = deref(self.thisptr).getGeometryIndexes()
        out = IndexList.from_index_list(_geom_indices)
        return out

    cpdef IndexList getPointIndexes(self) except+:
        cdef _index_list _point_indices
        with nogil:
            _point_indices = deref(self.thisptr).getPointIndexes()
        out = IndexList.from_index_list(_point_indices)
        return out

    cpdef IndexList getLineStringIndexes(self) except+:
        cdef _index_list _linestr_indices
        with nogil:
            _linestr_indices = deref(self.thisptr).getLineStringIndexes()
        out = IndexList.from_index_list(_linestr_indices)
        return out

    cpdef IndexList getPolygonIndexes(self) except+:
        cdef _index_list _polygon_indices
        with nogil:
            _polygon_indices = deref(self.thisptr).getPolygonIndexes()
        out = IndexList.from_index_list(_polygon_indices)
        return out

    cpdef IndexList getPolygonSubIndexes(self, size_t index) except+:
        cdef _index_list _polygon_indices
        with nogil:
            _polygon_indices = deref(self.thisptr).getPolygonSubIndexes(index)
        out = IndexList.from_index_list(_polygon_indices)
        return out

    cdef _bbox_list_{{x_name}} getPolygonSubBounds(self, size_t index) except+:
        cdef _bbox_list_{{x_name}} _polygon_bounds
        with nogil:
            _polygon_bounds = deref(self.thisptr).getPolygonSubBounds(<size_t> index)
        return _polygon_bounds

    cpdef _Coordinate_{{x_name}} getCoordinate(self, size_t index) except+:
        cdef int i
        cdef _coordinate_{{x_name}} _out
        _out = deref(self.thisptr).getCoordinate(index)
        out = _Coordinate_{{x_name}}(0.0, 0.0)
        out.c_copy(_out)
        return out

    cpdef _Coordinate_{{x_name}} getPointCoordinate(self, size_t index) except+:
        cdef int i
        cdef _coordinate_{{x_name}} _out
        _out = deref(self.thisptr).getPointCoordinate(index)
        out = _Coordinate_{{x_name}}(0.0, 0.0)
        out.c_copy(_out)
        return out

    cpdef {{x_type}}[:, :] getLineStringCoordinates(self, size_t index) except+:
        cdef int i
        cdef vector[_coordinate_{{x_name}}] _out
        cdef {{x_type}}[:, :] out
        _out = deref(self.thisptr).getLineStringCoordinates(<size_t> index)
        {{if x_name == "d"}}
        out = np.zeros(shape=(_out.size(), 4), dtype=np.float64)
        {{else}}
        out = np.zeros(shape=(_out.size(), 4), dtype=np.float32)
        {{endif}}
        for i in range(<int>_out.size()):
            out[i, 0] = _out[i].p
            out[i, 1] = _out[i].q
            out[i, 2] = _out[i].r
            out[i, 3] = _out[i].s
        _out.clear()
        return out

    cpdef {{x_type}}[:, :] getPolygonCoordinates(self, size_t index) except+:
        cdef int i, ny
        cdef vector[Coordinate[{{x_type}}]] _out
        cdef {{x_type}}[:, :] out
        _out = deref(self.thisptr).getPolygonCoordinates(<int> index)
        ny = <int> _out.size()
        {{if x_name == "d"}}
        out = np.zeros(shape=(ny, 4), dtype=np.float64)
        {{else}}
        out = np.zeros(shape=(ny, 4), dtype=np.float32)
        {{endif}}
        for i in range(ny):
            out[i, 0] = _out[i].p
            out[i, 1] = _out[i].q
            out[i, 2] = _out[i].r
            out[i, 3] = _out[i].s
        _out.clear()
        return out

    cpdef void addProperty(self, string name) except+:
        deref(self.thisptr).addProperty(name)

    cpdef void removeProperty(self, string name) except+:
        deref(self.thisptr).removeProperty(name)

{{for type_name, data_type in ptypes}}

{{if type_name == "flt" or type_name == "dbl"}}
{{if type_name[0] == x_name}}
    cpdef void setProperty_{{type_name}}(self, cl_uint index, string name, {{data_type}} value) except+:
        deref(self.thisptr).setProperty[{{data_type}}](<cl_uint> index, <string>name, <{{data_type}}> value)

    cpdef void convertProperty_{{type_name}}(self, string name) except+:
        deref(self.thisptr).convertProperty[{{data_type}}](name)

    cpdef void setPropertyVector_{{type_name}}(self, cl_uint index, string name, {{data_type}}[:] value) except+:
        cdef vector[{{data_type}}] buf
        cdef size_t vec_size
        vec_size = value.shape[0]
        buf.resize(vec_size)
        buf.assign(&value[0], &value[0]+vec_size)
        deref(self.thisptr).setProperty[vector[{{data_type}}]](<cl_uint> index, <string>name, buf)

    cpdef {{data_type}} getProperty_{{type_name}}(self, cl_uint index, string name) except+:
        return deref(self.thisptr).getProperty[{{data_type}}](<cl_uint> index, <string>name)

    cdef vector[vector[{{data_type}}]] getPropertyVectors_{{type_name}}_vector(self, string name) except+:
        cdef vector[vector[{{data_type}}]] out = deref(self.thisptr).getPropertyVectors[vector[vector[{{data_type}}]]](name)
        return out

    cdef vector[{{data_type}}] getPropertyVector_{{type_name}}_vector(self, cl_uint idx, string name) except+:
        cdef vector[{{data_type}}] out = deref(self.thisptr).getPropertyVector[vector[{{data_type}}]](idx, name)
        return out

    cdef void _setPropertyValues_{{type_name}}(self, string name, vector[{{data_type}}] v) except+:
        deref(self.thisptr).setPropertyValues[vector[{{data_type}}]](name, v)

    cpdef void setPropertyValues_{{type_name}}(self, string name, {{data_type}}[:] value) except+:
        cdef vector[{{data_type}}] buf
        cdef size_t vec_size
        vec_size = value.shape[0]
        buf.resize(vec_size)
        buf.assign(&value[0], &value[0]+vec_size)
        self._setPropertyValues_{{type_name}}(<string> name, buf)

    cpdef {{data_type}}[:] getPropertyVector_{{type_name}}(self, uint32_t idx, string name) except+:
        cdef vector[{{data_type}}] _out
        cdef {{data_type}}[:] out
        _out = self.getPropertyVector_{{type_name}}_vector(idx, name)
        {{if type_name == "flt"}}
        out = np.zeros(shape=_out.size(), dtype=np.float32)
        {{elif type_name == "dbl"}}
        out = np.zeros(shape=_out.size(), dtype=np.float64)
        {{endif}}
        memcpy(&out[0], _out.data(), sizeof({{data_type}}) * _out.size())
        return out
{{endif}}
{{else}}

{{if type_name == "str" }}
    cdef void _setPropertyValues_{{type_name}}(self, string name, vector[{{data_type}}] v) except+:
        deref(self.thisptr).setPropertyValues[vector[{{data_type}}]](name, v)

    def setPropertyValues_{{type_name}}(self, string name, object value):
        cdef vector[{{data_type}}] buf
        cdef size_t vec_size, i
        vec_size = len(value)
        for i in range(vec_size):
            buf.push_back(value[i])
        self._setPropertyValues_{{type_name}}(<string> name, buf)

{{else}}
    cdef void _setPropertyValues_{{type_name}}(self, string name, vector[{{data_type}}] v) except+:
        deref(self.thisptr).setPropertyValues[vector[{{data_type}}]](name, v)

    cpdef void setPropertyValues_{{type_name}}(self, string name, {{data_type}}[:] value) except+:
        cdef vector[{{data_type}}] buf
        cdef size_t vec_size
        vec_size = value.shape[0]
        buf.resize(vec_size)
        buf.assign(&value[0], &value[0]+vec_size)
        self._setPropertyValues_{{type_name}}(<string> name, buf)
{{endif}}

    cpdef void setProperty_{{type_name}}(self, cl_uint index, string name, {{data_type}} value) except+:
        deref(self.thisptr).setProperty[{{data_type}}](<cl_uint> index, <string>name, <{{data_type}}> value)

    cpdef void convertProperty_{{type_name}}(self, string name) except+:
        deref(self.thisptr).convertProperty[{{data_type}}](name)

    cpdef {{data_type}} getProperty_{{type_name}}(self, cl_uint index, string name) except+:
        return deref(self.thisptr).getProperty[{{data_type}}](<cl_uint> index, <string>name)
{{endif}}

{{endfor}}

    cpdef bool hasProperty(self, string prop_name) except+:
        return deref(self.thisptr).hasProperty(prop_name)

    cpdef _PropertyMap getProperties(self) except+:
        return _PropertyMap.c_copy(deref(self.thisptr).getProperties())

    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_):
        deref(self.thisptr).setProjectionParameters(deref(proj_.thisptr))

    cpdef _ProjectionParameters_d getProjectionParameters(self):
        out = _ProjectionParameters_d()
        out.c_copy(deref(self.thisptr).getProjectionParameters())
        return out

    cpdef _Vector_{{x_name}} convert(self, _ProjectionParameters_d this):
        out = _Vector_{{x_name}}()
        out.c_copy(deref(self.thisptr).convert(deref(this.thisptr)))
        return out

    cpdef _Vector_{{x_name}} region(self, _BoundingBox_{{x_name}} other, size_t parameter) except+:
        out = _Vector_{{x_name}}()
        out.c_copy(deref(self.thisptr).region(deref(other.thisptr), parameter))
        return out

    cpdef _Vector_{{x_name}} nearest(self, _BoundingBox_{{x_name}} other, size_t parameter) except+:
        out = _Vector_{{x_name}}()
        out.c_copy(deref(self.thisptr).nearest(deref(other.thisptr), parameter))
        return out

    cpdef _Vector_{{x_name}} attached(self, _Coordinate_{{x_name}} other, size_t parameter) except+:
        cdef vector[shared_ptr[GeometryBase[{{x_type}}]]] geom_base_ptr
        out = _Vector_{{x_name}}()
        geom_base_ptr = deref(self.thisptr).attached(deref(other.thisptr), parameter)
        return out

    def deduplicateVertices(self):
        deref(self.thisptr).deduplicateVertices()

    cpdef _cyRaster_{{x_name}} mapDistanceOnBounds(self, {{x_type}} resolution, string script,
                                                   size_t geom_type,
                                                   _BoundingBox_{{x_name}} bounds) except+:
        cdef Raster[{{x_type}}, {{x_type}}] _out
        out = _cyRaster_{{x_name}}()
        with nogil:
            _out = deref(self.thisptr).mapDistanceOnBounds[{{x_type}}](resolution,
                                                                       script,
                                                                       geom_type,
                                                                       deref(bounds.thisptr))
        out.c_rastercopy(_out)
        return out

    cpdef _cyRaster_{{x_name}} mapDistanceOnRaster(self, _cyRaster_{{x_name}} r, string script,
                                                   size_t geom_type) except+:
        cdef Raster[{{x_type}}, {{x_type}}] _out
        out = _cyRaster_{{x_name}}()
        with nogil:
            _out = deref(self.thisptr).mapDistanceOnRaster[{{x_type}}](deref(r.baseptr),
                                                                       script, geom_type)
        out.c_rastercopy(_out)
        return out

    cpdef _cyRaster_{{x_name}} rasterise(self, {{x_type}} resolution, string script,
                                         size_t geom_type, _BoundingBox_{{x_name}} bounds) except+:
        cdef Raster[{{x_type}}, {{x_type}}] _out
        out = _cyRaster_{{x_name}}()
        with nogil:
            _out = deref(self.thisptr).rasterise[{{x_type}}](resolution, script,
                                                             geom_type, deref(bounds.thisptr))
        out.c_rastercopy(_out)
        return out

    cpdef void pointSample(self, _cyRaster_{{x_name}} r) except+:
        deref(self.thisptr).pointSample[{{x_type}}](deref(r.sh_ptr))

    cpdef _BoundingBox_{{x_name}} getBounds(self) except+:
        _out = _BoundingBox_{{x_name}}()
        _out.c_copy(deref(self.thisptr).getBounds())
        return _out

    cpdef size_t getGeometryBaseCount(self) except+:
        return deref(self.thisptr).getGeometryBaseCount()

    cpdef size_t getPointCount(self) except+:
        return deref(self.thisptr).getPointCount()

    cpdef size_t getLineStringCount(self) except+:
        return deref(self.thisptr).getLineStringCount()

    cpdef size_t getPolygonCount(self) except+:
        return deref(self.thisptr).getPolygonCount()

    cpdef size_t getVertexSize(self)  except+:
        return deref(self.thisptr).getVertexSize()

    cpdef bool hasData(self) except+:
        cdef bool out
        out = deref(self.thisptr).hasData()
        return out

    cpdef void runScript(self, string script) except+:
        deref(self.thisptr).runScript(script)

    def __eq__(self, _Vector_{{x_name}} other):
        cdef bool out
        out = self.thisptr == other.thisptr
        return out

    def __ne__(self, _Vector_{{x_name}} other):
        cdef bool out
        out = self.thisptr != other.thisptr
        return out

    def __iadd__(self, _Vector_{{x_name}} other):
        deref(self.thisptr).iadd(deref(other.thisptr))
        return self

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        self.thisptr.reset()

{{endfor}}
