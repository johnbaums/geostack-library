# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t, uint64_t
from libc.stdio cimport printf
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..vector._cy_vector cimport Vector
from ..raster._cy_raster cimport (_cyRaster_d, _cyRaster_f,
                                  _cyRaster_d_i, _cyRaster_f_i,
                                  Raster, RasterBase)
from ..vector._cy_vector cimport _Vector_d, _Vector_f
from ..raster._cy_raster cimport _RasterPtrList_d, _RasterPtrList_f
from ..core._cy_variables cimport (VariablesBase, Variables,
                                   _Variables_f, _Variables_d,
                                   _Variables_i)
from ..core._cy_property cimport PropertyMap

np.import_array()

cdef extern from "gs_level_set.h" namespace "Geostack::LevelSetLayers":
    cdef enum LevelSetLayersType "Type":
        Distance
        DistanceUpdate
        Rate
        Speed
        Arrival
        Advect_x
        Advect_y
        LevelSetLayers_END

cdef extern from "gs_level_set.h" namespace "Geostack":
    cdef cppclass LevelSetParameters[T]:
        T time
        T dt
        T maxSpeed
        T area
        T bandWidth
        T JulianDate
        T JulianFraction

    cdef cppclass LevelSet[T]:
        LevelSet() except +
        bool init(string jsonStartConditions,
            Vector[T] &sources,
            shared_ptr[Variables[T, string]] variables,
            vector[shared_ptr[RasterBase[T]]] input_layers,
            vector[shared_ptr[RasterBase[T]]] output_layers) nogil except +
        bool step() nogil except +
        Raster[T, T]& getDistance() except +
        Raster[T, T]& getArrival() except +
        Raster[uint32_t, T]& getClassification() except +
        Raster[T, T]& getAdvect_x() except +
        Raster[T, T]& getAdvect_y() except +
        LevelSetParameters[T]& getParameters() except +
        uint64_t getEpochMilliseconds() except +
        void addSource(Vector[T] &v) except +

ctypedef LevelSetParameters[double] levelSetParams_d
ctypedef LevelSetParameters[float] levelSetParams_f

include "_cy_level_set.pxi"
