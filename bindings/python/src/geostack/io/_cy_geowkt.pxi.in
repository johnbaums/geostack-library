"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

rtypes = [('d', 'double'),
         ('f', 'float'),]

}}

{{for x_name, x_type in rtypes}}

cdef class geoWKT_{{x_name}}:

    @staticmethod
    cdef _Vector_{{x_name}} _geoWKTToVector(string geoWKTStr) except+:
        cdef Vector[{{x_type}}] _out
        _out = GeoWKT[{{x_type}}].geoWKTToVector(geoWKTStr)

        out = _Vector_{{x_name}}()
        out.c_copy(_out)
        return out

    @staticmethod
    def geoWKTToVector(geoWKTStr):
        if isinstance(geoWKTStr, str):
            out = geoWKT_{{x_name}}._geoWKTToVector(<bytes>geoWKTStr.encode('utf-8'))
        elif isinstance(geoWKTStr, bytes):
            out = geoWKT_{{x_name}}._geoWKTToVector(geoWKTStr)
        return out

    @staticmethod
    cdef _Vector_{{x_name}} _geoWKTFileToVector(string geoWKTFile) except+:
        cdef Vector[{{x_type}}] _out
        _out = GeoWKT[{{x_type}}].geoWKTFileToVector(geoWKTFile)

        out = _Vector_{{x_name}}()
        out.c_copy(_out)
        return out

    @staticmethod
    def geoWKTFileToVector(geoWKTFile):
        if isinstance(geoWKTFile, str):
            out = geoWKT_{{x_name}}._geoWKTFileToVector(<bytes>geoWKTFile.encode('utf-8'))
        elif isinstance(geoWKTFile, bytes):
            out = geoWKT_{{x_name}}._geoWKTFileToVector(geoWKTFile)
        return out

    @staticmethod
    cdef vector[cl_uint] _parseString(Vector[{{x_type}}]& v, string geoWKTstr) except+:
        cdef vector[cl_uint] idx
        idx = GeoWKT[{{x_type}}].parseString(v, geoWKTstr)
        return idx

    @staticmethod
    def parseString(_Vector_{{x_name}} v, bytes geoWKTstr):
        cdef vector[cl_uint] idx
        cdef uint32_t[:] out

        idx = geoWKT_{{x_name}}._parseString(deref(v.thisptr), <string> geoWKTstr)

        out = np.zeros(shape=idx.size(), dtype=np.uint32)
        memcpy(&out[0], idx.data(), idx.size() * sizeof(cl_uint))
        return out

    @staticmethod
    cdef vector[cl_uint] _parseStrings(Vector[{{x_type}}]& v, vector[string] geoWKTstr) except+:
        cdef vector[cl_uint] idxes
        idxes = GeoWKT[{{x_type}}].parseStrings(v, geoWKTstr)
        return idxes

    @staticmethod
    def parseStrings(_Vector_{{x_name}} v, object geoWKTstr):
        cdef vector[cl_uint] idx
        cdef vector[string] buf
        cdef int i
        cdef uint32_t[:] out

        assert isinstance(geoWKTstr, (list, tuple))

        for i in range(len(geoWKTstr)):
            buf.push_back(geoWKTstr[i])

        idx = geoWKT_{{x_name}}._parseStrings(deref(v.thisptr), buf)

        out = np.zeros(shape=idx.size(), dtype=np.uint32)
        memcpy(&out[0], idx.data(), idx.size() * sizeof(cl_uint))
        return out

    @staticmethod
    cdef string _vectorToGeoWKT(_Vector_{{x_name}} v) except+:
        cdef string out
        out = GeoWKT[{{x_type}}].vectorToGeoWKT(deref(v.thisptr))
        return out

    @staticmethod
    def vectorToGeoWKT(v):
        out = geoWKT_{{x_name}}._vectorToGeoWKT(v)
        return out

    @staticmethod
    cdef string _geometryToGeoWKT(_Vector_{{x_name}} v, size_t index) except+:
        cdef string out
        out = GeoWKT[{{x_type}}].vectorItemToGeoWKT(deref(v.thisptr), index)
        return out

    @staticmethod
    def vectorItemToGeoWKT(v, size_t index):
        out = geoWKT_{{x_name}}._geometryToGeoWKT(v, index)
        return out

{{endfor}}