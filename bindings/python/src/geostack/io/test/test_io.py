# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
import json
import numpy as np
from time import time
sys.path.insert(0, os.path.realpath('../../../'))

from geostack.io import AsciiHandler, GsrHandler
from geostack.io import FltHandler, GeoTIFFHandler
from geostack.raster import Raster
from geostack.utils import get_epsg


def test_ascii(tmpdir):
    file_name = tmpdir.join("testRaster.asc")
    testA = Raster(name="testA")
    testA.init(10, 1.0, ny=10, hy=1.0)
    testA.setAllCellValues(2.0)

    asc = AsciiHandler()
    asc.write(file_name.strpath, "", input_raster=testA)
    asc.read(file_name.strpath)

    assert np.allclose(asc.raster.data, testA.data)


def test_flt(tmpdir):
    file_name = tmpdir.join("testRaster.flt")
    testA = Raster(name="testA")
    testA.init(10, 1.0, ny=10, hy=1.0)
    testA.setAllCellValues(2.0)

    flt = FltHandler()
    flt.write(file_name.strpath, "", input_raster=testA)
    flt.read(file_name.strpath)

    assert np.allclose(flt.raster.data, testA.data)


def test_gsr(tmpdir):
    file_name = tmpdir.join("testRaster.gsr")
    testA = Raster(name="testA")
    testA.init(10, 1.0, ny=10, hy=1.0)
    testA.setAllCellValues(2.0)

    gsr = GsrHandler()
    gsr.write(file_name.strpath, "", input_raster=testA)
    del gsr
    gsr = GsrHandler()
    gsr.read(file_name.strpath)

    assert np.allclose(gsr.raster.data, testA.data)
    del gsr


def test_tiff(tmpdir):
    file_name = tmpdir.join("testRaster.tif")
    testA = Raster(name="testA")
    testA.init(10, 1.0, ny=10, hy=1.0)
    testA.setAllCellValues(2.0)
    testA.setProjectionParameters(get_epsg(4283))

    tif = GeoTIFFHandler()
    tif.write(file_name.strpath, "", input_raster=testA)
    del tif
    tif = GeoTIFFHandler()
    tif.read(file_name.strpath)

    assert np.allclose(tif.raster.data, testA.data)
    del tif
