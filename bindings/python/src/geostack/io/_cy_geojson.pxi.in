"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

rtypes = [('d', 'double'),
         ('f', 'float'),]

}}

{{for x_name, x_type in rtypes}}

cdef class geoJson_{{x_name}}:

    @staticmethod
    cdef _Vector_{{x_name}} _geoJsonToVector(string geoJson, bool enforceProjection=True) except+:
        cdef Vector[{{x_type}}] _out
        _out = GeoJson[{{x_type}}].geoJsonToVector(geoJson, enforceProjection)

        out = _Vector_{{x_name}}()
        out.c_copy(_out)
        return out

    @staticmethod
    def geoJsonToVector(geoJson, enforceProjection):
        if isinstance(geoJson, str):
            out = geoJson_{{x_name}}._geoJsonToVector(<bytes>geoJson.encode('utf-8'), enforceProjection)
        elif isinstance(geoJson, bytes):
            out = geoJson_{{x_name}}._geoJsonToVector(geoJson, enforceProjection)
        return out

    @staticmethod
    cdef _Vector_{{x_name}} _geoJsonFileToVector(string geoJson, bool enforceProjection=True) except+:
        cdef Vector[{{x_type}}] _out
        _out = GeoJson[{{x_type}}].geoJsonFileToVector(geoJson, enforceProjection)

        out = _Vector_{{x_name}}()
        out.c_copy(_out)
        return out

    @staticmethod
    def geoJsonFileToVector(geoJson, enforceProjection):
        if isinstance(geoJson, str):
            out = geoJson_{{x_name}}._geoJsonFileToVector(<bytes>geoJson.encode('utf-8'), enforceProjection)
        elif isinstance(geoJson, bytes):
            out = geoJson_{{x_name}}._geoJsonFileToVector(geoJson, enforceProjection)
        return out

    @staticmethod
    cdef string _vectorToGeoJson(_Vector_{{x_name}} v, bool enforceProjection=True,
                                 bool writeNullProperties=True) except+:
        cdef string out
        out = GeoJson[{{x_type}}].vectorToGeoJson(deref(v.thisptr), enforceProjection,
                                                  writeNullProperties)
        return out

    @staticmethod
    def vectorToGeoJson(v, enforceProjection, writeNullProperties):
        out = geoJson_{{x_name}}._vectorToGeoJson(v, enforceProjection, writeNullProperties)
        return out

{{endfor}}
