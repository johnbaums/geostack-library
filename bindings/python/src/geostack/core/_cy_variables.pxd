# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3

from cython.operator import dereference as deref
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.map cimport map as cpp_map
from libcpp.set cimport set as cpp_set
from libcpp.set cimport pair as cpp_pair
from libcpp.vector cimport vector
from libc.stdint cimport uint16_t, uint32_t, int32_t, uint64_t
from libcpp.functional cimport function
from libcpp.iterator cimport iterator
import numpy as np
cimport cython
cimport numpy as np
from libcpp.memory cimport shared_ptr, unique_ptr, make_shared

np.import_array()

ctypedef uint16_t cl_uint16
ctypedef uint32_t cl_uint

cdef extern from "gs_variables.h" namespace "Geostack":

    cdef cppclass VariablesBase[C]:
        pass

    cdef cppclass Variables[R, C](VariablesBase[C]):
        Variables() nogil except+
        C set(C key, R value) nogil except+
        R get(C key) nogil except+
        cpp_map[C, size_t]& getIndexes() nogil except+
        bool hasData() nogil except+

cdef class _Variables_f:
    cdef shared_ptr[Variables[float, string]] thisptr
    cpdef float get(self, string name) except+
    cpdef void set(self, string name, float value) except+
    cpdef dict getIndexes(self)
    cpdef bool hasData(self) except+

cdef class _Variables_d:
    cdef shared_ptr[Variables[double, string]] thisptr
    cpdef double get(self, string name) except+
    cpdef void set(self, string name, double value) except+
    cpdef dict getIndexes(self)
    cpdef bool hasData(self) except+

cdef class _Variables_i:
    cdef shared_ptr[Variables[cl_uint, uint32_t]] thisptr
    cpdef cl_uint get(self, uint32_t name) except+
    cpdef void set(self, uint32_t name, cl_uint value) except+
    cpdef dict getIndexes(self)
    cpdef bool hasData(self) except+
