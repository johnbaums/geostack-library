# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os
import sys

sys.path.insert(0, os.path.realpath('../../../'))
import numpy as np
from geostack.core import Variables
import pytest

def test_1():
    var = Variables()
    assert var.hasData() == False

def test_2():
    var = Variables()
    var.set("varA", 77.7)
    var.set("varB", 88.8)
    test_data = var.hasData()
    test_a = round(var.get("varA"), ndigits=1) == 77.7
    test_b = round(var.get("varB"), ndigits=1) == 88.8

    assert test_data == test_a == test_b == True
