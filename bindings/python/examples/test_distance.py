# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import sys

from geostack.vector import Vector
from geostack.raster import Raster
from geostack.runner import runScript, runVectorScript

# Create raster layer
A = Raster(name = "A")

# Initialize Raster
A.init(
    nx = 200, ny = 200, nz = 2, 
    hx = 0.01, hy = 0.01, hz = 1, 
    ox = -1, oy = -1, oz = 0)

# Create vectors
v1 = Vector()
v1.addPoint( [0.5, 0.5, 0.0] )

v2 = Vector()
v2.addPoint( [-0.5, -0.5, 0.0] )

# Distance map
A.mapVector(v1, level = 0)
A.mapVector(v2, level = 1)

# Write Raster
A.write('./_out_distance.tiff')
    
print("Done")

