# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import json
import numpy as np
import matplotlib.pyplot as plt

from geostack.vector import Vector
from geostack.solvers import Particle

# Parameters for Ornstein–Uhlenbeck
N = 1000
N_show = 5
steps = 1000
t_end = 10

# Create initial conditions
p = Vector()
for i in range (0, N):
    p.addPoint( [0, 0, 0, 0] )

# Create solver
dt = float(t_end)/steps
config = {
    "dt": dt,
    "updateScript" : [f'''
        mu.x = -0.7*position.x;
    ''', f'''
        sigma.x = 0.06;
    ''']
}
solver = Particle()
solver.init(json.dumps(config), p)

# Create value lists
t = np.linspace(0, t_end, steps+1)
c = [[] for i in range(0, N_show)]
for i in range(0, N_show):
    c[i].append(p.getCoordinate(i)[0]) 

# Run solver  
for step in range(0, steps):

    # Take step
    solver.step()
    
    # Built lists
    for i in range(0, N_show):
        c[i].append(p.getCoordinate(i)[0])

# Plot
plt.xlabel("time (s)")
plt.ylabel("y")
cm = plt.get_cmap("viridis") 
for i in range(0, N_show):
    plt.plot(t, c[i], color=plt.cm.viridis(i/(N_show-1)))
plt.show()

print("Done")