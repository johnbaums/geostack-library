# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import json
import numpy as np
import matplotlib.pyplot as plt
from geostack.raster import Raster
from geostack.runner import runScript

# Create raster layers
f = Raster(name = "f")
ft = Raster(name = "ft")
fm = Raster(name = "fm")

# Initialize raster layers
nz = 100
f.init(nx = 100, ny = 100, nz = nz, hx = 1.0, hy = 1.0, hz = 1.0)
ft.init(nx = 100, ny = 100, nz = nz/2, hx = 1.0, hy = 1.0, hz = 1.0)
fm.init(nx = 100, ny = 100, hx = 1.0, hy = 1.0)

# Populate data
runScript('''
    f = sin(M_PI*x*y*z/20000.0);
''', [f])

# Find magnitude of Fourier transform
runScript('''
    
    REAL f_re = 0.0;
    REAL f_im = 0.0;
    REAL c = 2.0*M_PI*kpos/(REAL)f_layers;
    for (uint k = 0; k < f_layers; k++) {
        f_re += f[k]*cos(c*k);
        f_im -= f[k]*sin(c*k);
    }
    ft = hypot(f_re, f_im);
    
''', [ft, f])

# Find maximum frequency of Fourier transform
runScript('''
    
    uint ft_idx = 0;
    REAL ft_max = ft[0];
    for (uint k = 0; k < ft_layers; k++) {
        REAL ft_k = ft[k];
        
        // Check for maximum
        if (ft_k > ft_max) {
        
            // Update maximum and index
            ft_max = ft_k;
            ft_idx = k;
        }
    }
    
    // Store index
    fm = ft_idx;
    
''', [fm, ft])

# Write
#fm.write("__out_fm.tif")

# Plot power spectrum
x = np.linspace(0, 1, 50)
for p in (0, 19, 39, 59, 79, 99):
    y = ft.data[:, p, p]
    plt.plot(x, y, label = f"x: {p}, y: {p}")
plt.legend(loc="top right")    
plt.show()

# Plot maximum
plt.imshow(fm.data, cmap=plt.cm.viridis)
plt.show()

print("Done")