# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
 
import sys
import json
import numpy as np

from geostack.raster import Raster, RasterPtrList
from geostack.vector import Vector
from geostack.runner import runScript
from geostack.solvers import LevelSet
from geostack.core import Variables
from geostack.io import vectorToGeoJson

# Create raster layer
type = Raster(name = "type")

# Initialize Raster
type.init(nx = 1000, ny = 1000, hx = 1.0, hy = 1.0, ox = -500, oy = -500)
runScript("type = x > 10 && x < 110 && y > 10 && y < 110 ? 2 : 1;", [type])
type.write('./_out_level_set_class_type.tiff')

# Create vector layer
v = Vector()
point_id = v.addPoint( [0, 0] )
v.setProperty(point_id, "radius", 10)

# Create solver
solver = LevelSet()

# create Spark solver configuration
solverConfig = {
    "resolution": 1.0,
    "buildScript": '''
        if (class == 1) { speed = 0.5;}
        else if (class == 2) { speed = 2.0;}
    ''',
    "initialisationScript": "class = type;"
}

# Create input list
inputLayers = RasterPtrList()
inputLayers.append(type)

if not solver.init(json.dumps(solverConfig), v, inputLayers = inputLayers):
    raise RuntimeError("Unable to initialise solver.")
    
print("Running ...")
while solver.getParameters()['time'] <= 100.0:
    if not solver.step():
        break
        
solver.getArrival().write('./_out_level_set_class_arrival.tiff')
solver.getClassification().write('./_out_level_set_class_class.tiff')
solver.getDistance().write('./_out_level_set_class_distance.tiff')
        
# Generate isochrones
isochroneVector = solver.getArrival().vectorise(
    np.arange(0, 100, 10), 100)

# Write isochrone file
with open('./_out_level_set_class_iso.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(isochroneVector, enforceProjection = False))
            
print("Done")