# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from geostack.raster import Raster
from geostack.runner import runScript, runVectorScript
from geostack.io import geoJsonToVector
from geostack.gs_enums import ReductionType

geojson = '''{"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"}, 
            "properties": {"C": 10}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"}, 
            "properties": {"C": 20}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"}, 
            "properties": {"C": 30}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"}, 
            "properties": {"C": 40}, "type": "Feature"}, 
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"}, 
            "properties": {"C": 50}, "type": "Feature"},
        {"geometry": {"coordinates": [2, 0.75], "type": "Point"}, 
            "properties": {"C": 60}, "type": "Feature"}
        ], "type": "FeatureCollection"}'''
     
v = geoJsonToVector(geojson, False)
v.addProperty("A")
v.addProperty("B")

r = Raster(name="r")
delta = 0.02;
r.init(nx = 150, ny = 150, hx = delta, hy = delta)

runScript("r = x;", [ r ])

runVectorScript("A = r;", v, [ r ], ReductionType.Minimum)
runVectorScript("B = r;", v, [ r ], ReductionType.Maximum)

for i in range(0, 6):
    print(f"A: {v.getProperty(i, 'A')}, B: {v.getProperty(i, 'B')}, C: {v.getProperty(i, 'C')}")