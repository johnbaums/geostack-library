# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

cmake_minimum_required(VERSION 3.8)

# Project name
project(Geostack VERSION 0.2.9 LANGUAGES CXX)

# Ensure this is an out-of-source build
if("${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}")
    message(FATAL_ERROR "Build must be in a separate directory to source")
endif()

# Set default build type to Release
if (NOT CMAKE_BUILD_TYPE OR CMAKE_BUILD_TYPE STREQUAL "")
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING "" FORCE)
endif()

# Add REAL definition of double or float
set(OpenCL_FLOAT_TYPE "float" CACHE STRING "")
if(OpenCL_FLOAT_TYPE STREQUAL "double")
    add_definitions( -DREAL=double )
    add_definitions( -DREAL_DOUBLE )
	message(STATUS "Using double precision OpenCL")
elseif(OpenCL_FLOAT_TYPE STREQUAL "float")
    add_definitions( -DREAL=float )
    add_definitions( -DREAL_FLOAT )
	message(STATUS "Using single precision for OpenCL")
else()
    message(FATAL_ERROR "Float type must be either 'float' or 'double'" )
endif()

# Add math definitions
add_definitions( -D_USE_MATH_DEFINES )

# Find libraries
find_package(OpenCL REQUIRED)
if(UNIX AND NOT APPLE)
    find_package(Threads REQUIRED)
endif()
    
# Explicitly set INSTALL project to run in Visual Studio
set(CMAKE_VS_INCLUDE_INSTALL_TO_DEFAULT_BUILD 1)

# Set up installation directories
include(GNUInstallDirs)

# Build library
add_subdirectory(lib)

