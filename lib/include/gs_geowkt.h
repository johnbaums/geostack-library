/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef GEOSTACK_GEOWKT_H
#define GEOSTACK_GEOWKT_H

#include <string>
#include <locale>

#include "json11.hpp"

#include "gs_vector.h"

namespace Geostack
{
    // Forward declarations
    template <typename T>
    class Vector;

    template <typename T>
    class GeoWKT {

    public:

        // Parse strings
        static std::vector<cl_uint> parseString(Geostack::Vector<T> &v, std::string geoWKTStr);
        static std::vector<cl_uint> parseStrings(Geostack::Vector<T> &v, std::vector<std::string> geoWKTStr);

        // Parse from file
        static Geostack::Vector<T> geoWKTFileToVector(std::string geoWKTFile);

        // Parse from string
        static Geostack::Vector<T> geoWKTToVector(std::string geoWKTStr);

        // Write to string
        static std::string vectorToGeoWKT(Geostack::Vector<T> &v);

        // Write to string
        static std::string vectorItemToGeoWKT(Geostack::Vector<T> &v, std::size_t index);

    private:

        // Parse coordinate
        static Coordinate<T> parseCoordinate(std::string geoWKTCoordStr, std::locale &loc);
    };
}

#endif
