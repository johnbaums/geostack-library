/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_GEOJSON_H
#define GEOSTACK_GEOJSON_H

#include <string>

#include "json11.hpp"

#include "gs_vector.h"

namespace Geostack
{
    // Forward declarations
    template <typename T>
    class Vector;

    template <typename T>
    class GeoJson {

    public:

        // Parse from file
        static Geostack::Vector<T> geoJsonFileToVector(std::string geoJsonFile, bool enforceProjection = true);

        // Parse from string
        static Geostack::Vector<T> geoJsonToVector(std::string geoJsonStr, bool enforceProjection = true);

        // Write to string
        static std::string vectorToGeoJson(Geostack::Vector<T> &v, bool enforceProjection = true, bool writeNullProperties = false);

    private:

        // Parse coordinate
        static Coordinate<T> parseCoordinate(const std::vector<json11::Json> &coordinates, const std::map<std::string, json11::Json> &properties);

        // Parse string
        static bool parseString(Geostack::Vector<T> &v, std::string geoJsonStr);

        // Set projection
        static void setProjection(Vector<T> &v);

    };
}

#endif
