/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_LEVEL_SET_SOLVER_H
#define GEOSTACK_LEVEL_SET_SOLVER_H

#include <string>
#include <vector>
#include <set>

#include "date.h"
#include "gs_raster.h"
#include "gs_variables.h"

// Link to external resources
extern const char R_cl_date_head_c[];
extern const uint32_t R_cl_date_head_c_size;
extern const char R_cl_level_set_advect_c[];
extern const uint32_t R_cl_level_set_advect_c_size;
extern const char R_cl_level_set_head_c[];
extern const uint32_t R_cl_level_set_head_c_size;
extern const char R_cl_level_set_init_c[];
extern const uint32_t R_cl_level_set_init_c_size;
extern const char R_cl_level_set_build_c[];
extern const uint32_t R_cl_level_set_build_c_size;
extern const char R_cl_level_set_update_c[];
extern const uint32_t R_cl_level_set_update_c_size;
extern const char R_cl_level_set_reinit_c[];
extern const uint32_t R_cl_level_set_reinit_c_size;

namespace Geostack
{    
    /**
    * %LevelSetParameters structure
    */
    template <typename TYPE>
    struct alignas(8) LevelSetParameters {
    
        TYPE time;           // Current time
        TYPE dt;             // Current time step
        TYPE maxSpeed;       // Maximum speed in domain
        TYPE area;           // Area within perimeter
        TYPE bandWidth;      // Width of narrow band in world units
        TYPE JulianDate;     // Current Julian date
        TYPE JulianFraction; // Julian fraction for current day
    };

    /**
    * %VectorRasterIndexes class for indexes of %Raster in %Vector
    * with corresponding area weighting.
    */    
    template <typename TYPE>
    struct alignas(8) LevelSetRasterIndex {
        cl_ushort i;
        cl_ushort j;
        cl_ushort k;
        TYPE mag;
    };

    /**
    * %LevelSet layer types.
    */
    namespace LevelSetLayers {
        enum Type {
            Distance,           ///< Distance raster
            DistanceUpdate,     ///< Distance update raster
            Rate,               ///< Time integration rate raster
            Speed,              ///< Speed raster
            Arrival,            ///< Arrival time raster
            Advect_x,           ///< Advection vector x component
            Advect_y,           ///< Advection vector y component
            //StartTime,          ///< Start time raster
            //StartTimeUpdate,    ///< Start time update raster
            LevelSetLayers_END  ///< Placeholder for count of layers
        };
    }	
	
    /**
    * %LevelSet class for perimeter growth. 
    */
    template <typename TYPE>
    class LevelSet {

        public:
        
            /**
            * LevelSet constructor
            */
            LevelSet():initialised(false), p(), resolution((TYPE)0.0), levels(1) { }

            // Initialisation
            bool init(
                std::string jsonConfig, 
                Vector<TYPE> sources_, 
                std::shared_ptr<Variables<TYPE, std::string> > variables_ = nullptr,
                std::vector<RasterBasePtr<TYPE> > inputLayers_ = std::vector<RasterBasePtr<TYPE> >(),
                std::vector<RasterBasePtr<TYPE> > outputLayers_ = std::vector<RasterBasePtr<TYPE> >());

            // Execution
            bool step();

            // Add source to level set
            void addSource(const Vector<TYPE> &v);

            /**
            * Return level set classification %Raster
            */
            Raster<uint32_t, TYPE> &getClassification() { 
                return classbits;
            }

            /**
            * Return level set distance %Raster
            */
            Raster<TYPE, TYPE> &getDistance() { 
                return layers[LevelSetLayers::Distance];
            }

            /**
            * Return arrival time %Raster
            */
            Raster<TYPE, TYPE> &getArrival() { 
                return layers[LevelSetLayers::Arrival];
            }

            /**
            * Return advection x-component %Raster
            */
            Raster<TYPE, TYPE> &getAdvect_x() { 
                return layers[LevelSetLayers::Advect_x];
            }

            /**
            * Return advection y-component %Raster
            */
            Raster<TYPE, TYPE> &getAdvect_y() { 
                return layers[LevelSetLayers::Advect_y];
            }

            /**
            * Return solver %LevelSetParameters
            */
            LevelSetParameters<TYPE> &getParameters() { 
                return p;
            }

            /**
            * Return epoch time
            */
            uint64_t getEpochMilliseconds() { 
                return currentEpochMilliseconds;
            }

        private:

            // Static variables
            static const TYPE CFL;                   ///< CFL ratio
            static const TYPE timeStepMax;           ///< Maximum time step
            static const TYPE narrowBandCells;       ///< Level set narrow band width
            
            static std::string buildNormal;          ///< Definition of normal vector
            static std::string buildAdvectNormal;    ///< Definition of advective normal vector
            static std::string buildAdvectMag;       ///< Definition of advective vector magnitude
            static std::string buildAdvectVector;    ///< Definition of advective vector
            static std::string buildAdvectDotNormal; ///< Definition of dot product of advective normal and advective vector

            // Internal variables
            uint64_t iters;                          ///< Number of iterations
            LevelSetParameters<TYPE> p;              ///< Parameter structure
            uint32_t levels;                         ///< Number of simulation levels
            TYPE resolution;                         ///< Cell size in world units
            volatile bool initialised;               ///< Solver initialised
            TYPE lastTime;                           ///< Last time value
            TYPE timeMultiple;                       ///< Target time multiple
            Vector<TYPE> currentSources;             ///< Sources to be applied

            // Time and date variables
            bool hasStartDate;
            uint64_t currentEpochMilliseconds;
            uint64_t localZoneOffsetMinutes;
            date::sys_time<std::chrono::milliseconds> startTime;
            date::sys_time<std::chrono::milliseconds> startDay;

            // Internal raster layers
            Raster<uint32_t, TYPE> classbits;        ///< Classification structure: 1 bit state, 23 bits class and 8 bits sub-class
            std::vector<Raster<TYPE, TYPE> > layers; ///< User-defined input layers

            // Internal generators
            KernelRequirements reinitReq;            ///< Reinitialisation kernel raster requirements    

            KernelGenerator initKernelGenerator;     ///< Initialisation generator
            KernelGenerator advectKernelGenerator;   ///< Advection generator
            KernelGenerator buildKernelGenerator;    ///< Build generator
            KernelGenerator updateKernelGenerator;   ///< Update generator
            KernelGenerator reinitKernelGenerator;   ///< Reinit generator

            RasterBaseRefs<TYPE> initRefs;           ///< Initialisation kernel rasters
            RasterBaseRefs<TYPE> advectRefs;         ///< Advection kernel rasters
            RasterBaseRefs<TYPE> buildRefs;          ///< Build kernel rasters
            RasterBaseRefs<TYPE> updateRefs;         ///< Update kernel rasters
            RasterBaseRefs<TYPE> reinitRefs;         ///< Reinitialisation kernel rasters
            RasterBaseRefs<TYPE> reinitFlipRefs;     ///< Reinitialisation kernel flipped distance rasters
            
            // Internal kernels
            cl::Kernel initKernel;           ///< Initialisation kernel
            cl::Kernel advectKernel;         ///< Advection kernel for active tiles
            cl::Kernel advectInactiveKernel; ///< Advection kernel for inactive tiles
            cl::Kernel buildKernel;          ///< Build kernel
            cl::Kernel updateKernel;         ///< Update kernel for active tiles
            cl::Kernel updateInactiveKernel; ///< Update kernel for inactive tiles
            cl::Kernel reinitKernel;         ///< Reinitialisation kernel
            std::size_t buildKernelMultiple; ///< Optimal workgroup multiple of build kernel
            
            // Input data
            Vector<TYPE> sources;                                     ///< Level set sources
            std::shared_ptr<Variables<TYPE, std::string> > variables; ///< Pointer to user-defined variables
            std::vector<RasterBasePtr<TYPE> > inputLayers;            ///< User-defined input layers
            std::vector<RasterBasePtr<TYPE> > outputLayers;           ///< User-defined input 

            // Status data
            std::vector<cl_uint> statusVector;
            cl::Buffer statusBuffer;
            cl::Event statusMapEvent;

            // Raster index data
            std::vector<cl_uint> rasterIndexCount;   ///< Narrow band index count
            cl::Buffer rasterIndexCountBuffer;       ///< Narrow band index count buffer
            cl::Buffer rasterIndexBuffer;            ///< Narrow band index buffer
            cl::Event rasterIndexMapEvent;           ///< Narrow band index read event

            // Tile processing
            tileIndexSet newTileIndexes;             ///< Indexes of new tiles
            tileIndexSet activeTiles;                ///< Indexes of active tiles
            tileIndexSet inactiveTiles;              ///< Indexes of inactive tiles

            // Member functions
            Dimensions<TYPE> boundDimensions(Vector<TYPE> &v);
            void resizeTiles(uint32_t tx, uint32_t ty, uint32_t ox, uint32_t oy);
    };
}

#endif