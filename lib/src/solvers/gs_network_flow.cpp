/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <set>
#include <iostream>
#include <sstream>
#include <eigen/Sparse>

#include "json11.hpp"

#include "gs_property.h"
#include "gs_network_flow.h"

namespace Geostack
{

    using namespace json11;

    typedef Eigen::Triplet<double> Et;
    
    template <typename TYPE>
    bool NetworkFlowSolver<TYPE>::init(Vector<TYPE> &networkIn, std::string jsonConfig) {
    
        // Clear internal copy of network
        network.clear();
        linkPoints.clear();

        // Copy network projection
        network.setProjectionParameters(networkIn.getProjectionParameters());
        
        // Parse config
        Json config;
        if (!jsonConfig.empty()) {
            std::string err;
            config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }
        }

        double constant = 1.0;
        if (config["constant"].is_number()) {
            constant = config["constant"].number_value();
        }

        int defaultSegmentType = static_cast<int>(NetworkSegmentType::HazenWilliams);
        if (config["defaultLinkType"].is_number()) {
            defaultSegmentType = config["defaultLinkType"].int_value();
        }       

        TYPE defaultDiameter = 0.0;
        if (config["defaultDiameter"].is_number()) {
            defaultDiameter = (TYPE)config["defaultDiameter"].number_value();
        }

        // Get properties
        PropertyMap &properties = network.getProperties();
        PropertyMap &propertiesIn = networkIn.getProperties();
        bool hasType = propertiesIn.hasProperty("type");
        bool hasHead = propertiesIn.hasProperty("head");
        bool hasFlow = propertiesIn.hasProperty("flow");
        bool hasDiameter = propertiesIn.hasProperty("diameter");
        
        propertiesIn.addProperty("diameter");
        auto &propertyDiameter = propertiesIn.template getPropertyRef<std::vector<TYPE> >("diameter");

        // Copy points
        auto bounds = networkIn.getBounds();
        auto pointIndexes = networkIn.getPointIndexes();
        std::size_t nDuplicatePoints = 0;
        std::multimap<uint64_t, std::size_t> zIndexes;
        for (std::size_t k = 0; k < pointIndexes.size(); k++) {

            // Get coordinate
            auto c = networkIn.getPointCoordinate(pointIndexes[k]);

            // Get z-index of coordinate
            uint64_t zIndex = bounds.createZIndex2D(c);

            // Check for duplicate points
            bool noDuplicates = true;
            auto indexRange = zIndexes.equal_range(zIndex);
            for (auto it = indexRange.first; it != indexRange.second; ++it) {
                if (c == network.getPointCoordinate(it->second)) {
                    noDuplicates = false;
                }
            }

            if (noDuplicates) {
                auto id = network.addPoint(c);
                zIndexes.insert( { zIndex, id } );
                network.template setProperty<cl_uint>(id, "id", id);
                
                // Set default type as junction
                network.template setProperty<int>(id, "type", (int)NetworkNodeType::Junction);

                // Copy data
                if (hasHead) {                    
                    TYPE head = propertiesIn.template getPropertyFromVector<TYPE>("head", pointIndexes[k]);
                    network.template setProperty<TYPE>(id, "head", isValid<TYPE>(head) ? head : (TYPE)0.0);
                } else {
                    network.template setProperty<TYPE>(id, "head", (TYPE)0.0);
                }
                
                if (hasFlow) {                    
                    TYPE flow = propertiesIn.template getPropertyFromVector<TYPE>("flow", pointIndexes[k]);
                    network.template setProperty<TYPE>(id, "flow", isValid<TYPE>(flow) ? flow : (TYPE)0.0);
                } else {
                    network.template setProperty<TYPE>(id, "flow", (TYPE)0.0);
                }


            } else {
                nDuplicatePoints++;
            }
        }

        if (nDuplicatePoints > 0) {
            std::cout << "WARNING: " << nDuplicatePoints << " duplicate point" << 
                (nDuplicatePoints > 1 ? "s ignored" : " ignored") << std::endl;
        }

        // Copy line strings
        std::size_t nDuplicateLinks = 0;
        std::map<uint64_t, uint32_t> validLines;
        for (const auto &l : networkIn.getLineStringIndexes()) {

            // Get coordinates
            const auto &c = networkIn.getLineStringCoordinates(l);

            // Check number of points
            if (c.size() > 2) {
                throw std::runtime_error("Network must be segments composed of two vertices");
            }
                        
            // Update table of links to points
            uint32_t id[2];
            for (int i = 0; i < 2; i++) {
                
                // Set id to null value
                id[i] = getNullValue<uint32_t>();

                // Get z-index of coordinate
                uint64_t zIndex = bounds.createZIndex2D(c[i]);
                
                // Loop through geometry
                auto indexRange = zIndexes.equal_range(zIndex);
                for (auto it = indexRange.first; it != indexRange.second; ++it) {
                    if (c[i] == network.getPointCoordinate(it->second)) {
                        id[i] = it->second;
                        break;
                    }
                }
                
                // Add any missing points at the end of segments
                if (id[i] == getNullValue<uint32_t>()) {
                    id[i] = network.addPoint(c[i]);
                    zIndexes.insert( { zIndex, id[i] } );

                    // Set data
                    network.template setProperty<cl_uint>(id[i], "id", id[i]);
                    network.template setProperty<TYPE>(id[i], "head", (TYPE)0.0);
                    network.template setProperty<TYPE>(id[i], "flow", (TYPE)0.0);
                    network.template setProperty<int>(id[i], "type", (int)NetworkNodeType::Terminator);
                }
            }

            // Check for duplicate lines
            uint64_t lineKey01 = (((uint64_t)id[0])<<32) | (uint64_t)id[1];
            uint64_t lineKey10 = (((uint64_t)id[1])<<32) | (uint64_t)id[0];
            if (id[0] != id[1] && validLines.find(lineKey01) == validLines.end() && validLines.find(lineKey10) == validLines.end()) {

                // Add line
                validLines.insert( { lineKey01, l } );

            } else {
                nDuplicateLinks++;
            }
        }

        if (nDuplicateLinks > 0) {
            std::cout << "WARNING: " << nDuplicateLinks << " duplicate or bad link" << 
                (nDuplicateLinks > 1 ? "s ignored" : " ignored") << std::endl;
        }

        // Add non-duplicate lines
        for (auto &l : validLines) {
            auto id = network.addLineString(networkIn.getLineStringCoordinates(l.second));            
            
            // Set id
            network.template setProperty<cl_uint>(id, "id", id);

            // Set type
            int type = defaultSegmentType;
            if (hasType) {
                type = propertiesIn.template getPropertyFromVector<int>("type", l.second);
            }
            network.template setProperty<int>(id, "type", isValid<int>(type) ? type : defaultSegmentType);

            // Set diameter
            TYPE diameter = defaultDiameter;
            if (hasDiameter) {                    
                diameter = propertiesIn.template getPropertyFromVector<TYPE>("diameter", l.second);
            }
            network.template setProperty<TYPE>(id, "diameter", diameter);

            // Update id to new map
            l.second = id;
        }

        // Parse line strings
        for (auto &l : validLines) {
            const auto &c = network.getLineStringCoordinates(l.second);

            // Update table of links to points
            linkPoints.push_back( { (uint32_t)(l.first>>32), (uint32_t)(l.first&(uint64_t)0xFFFFFFFF) } );

            // Get diameter
            TYPE diameter = network.template getProperty<TYPE>(l.second, "diameter");
            if (diameter <= 0.0 || isInvalid<TYPE>(diameter)) {
                std::stringstream err;
                err << "Network segment " << l.second << " has zero or negative diameter";
                throw std::runtime_error(err.str());
            }
        
            // Calculate length
            TYPE length = std::hypot(c[0].p-c[1].p, c[0].q-c[1].q);
            network.template setProperty<TYPE>(l.second, "length", length);

            // Get type
            int type = properties.template getPropertyFromVector<int>("type", l.second);

            // Calculate constants
            TYPE K = 0.0;
            TYPE k = 0.0;
            switch (type) {

                case NetworkSegmentType::HazenWilliams: {

                    // Set pipe constants from Hazen-Williams formula.
                    // Q = (SK)^(1/1.852)
                    // K = (10.67 L) / (C^1.85 D^4.87)
                    K = (TYPE)(pow(constant, 1.85)*pow(diameter, 4.87))/(10.67*length);
                    k = (TYPE)(1.0/1.852);
                    }
                    break;
                
                case NetworkSegmentType::ManningOpenChannel: {

                    // Set pipe constants from Manning open-channel formula.
                    // Q = (SK)^(1/2)
                    // K = D^2.5 / (n^2 L)
                    K = (TYPE)pow(diameter, 2.5)/(constant*constant*length);
                    k = (TYPE)0.5;
                    }
                    break;
                
                case NetworkSegmentType::Logarithmic: {
            
                    // Set pipe constants for logarithmic flow relation
                    // Q = K ln(kS + 1)
                    K = (TYPE)diameter*1.0E4;
                    k = (TYPE)0.01;
                    }
                    break;
                
                case NetworkSegmentType::SqrtExp: {
            
                    // Set pipe constants for nonlinear flow relation
                    // Q = K sqrt(S) exp(-kS)
                    K = (TYPE)10.0;
                    k = (TYPE)0.006;
                    }
                    break;

                default: {
                    std::stringstream err;
                    err << "Unknown segment type " << type;
                    throw std::runtime_error(err.str());
                }
            }

            // Set constants
            network.template setProperty<TYPE>(l.second, "K", K);
            network.template setProperty<TYPE>(l.second, "k", k);
        }

        return true;
    }

    template <typename TYPE>
    bool NetworkFlowSolver<TYPE>::run() {
    
        // Get data handles        
        auto pointIndexes = network.getPointIndexes();
        auto lineStringIndexes = network.getLineStringIndexes();

        // Get property handles
        PropertyMap &properties = network.getProperties();
        auto &typeVector = properties.template getPropertyRef<std::vector<int> >("type");
        auto &headVector = properties.template getPropertyRef<std::vector<TYPE> >("head");
        auto &flowVector = properties.template getPropertyRef<std::vector<TYPE> >("flow");

        // Create auxillary vectors 
        std::size_t N = pointIndexes.size();
        std::vector<double> b0(N);         // Initial forcing vector
        std::vector<double> d(N);          // Diagonal vector
        std::set<std::size_t> terminators; // Termination points

        // Create vectors and matrices
        std::vector<Et> c;
        Eigen::SparseMatrix<double> A(N, N);
        Eigen::Matrix<double, Eigen::Dynamic, 1> b(N);  // Forcing vector
        Eigen::Matrix<double, Eigen::Dynamic, 1> h(N);  // Pressure vector
        Eigen::Matrix<double, Eigen::Dynamic, 1> dh(N); // Pressure update vector
        
        // Set data
        double inflow = 0.0;
        for (std::size_t k = 0; k < N; k++) {
            
            // Get point index
            auto &p = pointIndexes[k];

            // Set flow and pressure
            h(k) = 0.0;
            b0[k] = 0.0;
            if (typeVector[p] == NetworkNodeType::Terminator) {

                // Get pressure from terminators
                h(k) = (double)headVector[p];
                terminators.insert(k);

            } else {

                // Get inflow
                double flow = (double)flowVector[p];

                // Update initial forcing vector
                b0[k] = flow;
                inflow += flow;
            }
            
        }

        // Iterate
        std::size_t iter = 0;
        double qval, dval;
        double eps = 1.0E-12;
        double dh_dot, dh0_dot = 1.0;
        do {

            // Clear data
            c.clear();
            for (std::size_t k = 0; k < N; k++) {

                // Set forcing
                b(k) = b0[k];

                // Reset diagonal
                d[k] = 0.0;
            }

            // Loop over links
            for (std::size_t k = 0; k < lineStringIndexes.size(); k++) {
                
                // Get link index
                auto &l = lineStringIndexes[k];

                // Get link parameters
                NetworkSegmentType::Type type = static_cast<NetworkSegmentType::Type>(typeVector[l]);
                double lK = (double)network.template getProperty<TYPE>(l, "K");
                double lk = (double)network.template getProperty<TYPE>(l, "k");

                // Get indexes
                cl_uint i = linkPoints[k].first;
                cl_uint j = linkPoints[k].second;

                // Get h values from link
                double hi = h(i);
                double hj = h(j);
                double hval = fabs(hi-hj);
                double hsign = hi-hj > 0.0 ? 1.0 : -1.0;

                // Calculate matrix entries
                qval = 0.0;
                dval = 0.0;
                switch (type) {
                            
                    case NetworkSegmentType::HazenWilliams: {

                        qval = hsign*pow(hval*lK, lk);
                        dval = pow(lK, lk)*pow((hval+eps), (lk-1.0))*lk; 

                        } break;
                            
                    case NetworkSegmentType::ManningOpenChannel: {

                        qval = hsign*pow(hval*lK, lk);
                        dval = pow(lK, lk)*pow((hval+eps), (lk-1.0))*lk; 

                        } break;
                            
                    case NetworkSegmentType::Logarithmic: {
                                
                        qval = hsign*lK*log(lk*hval+1.0);
                        dval = lK*lk/(lk*hval+1.0); 

                        } break;
                            
                    case NetworkSegmentType::SqrtExp: {
                                
                        qval = hsign*sqrt(lK*hval+eps)*exp(-lk*lK*hval);
                        dval = 0.5*lK*exp(-lk*lK*hval)*(1.0-2.0*lk*lK*hval)/sqrt(lK*hval+eps);

                        } break;
                        
                    default: {
                        std::stringstream err;
                        err << "Unknown segment type " << type;
                        throw std::runtime_error(err.str());
                    }
                }

                // Set matrix entries
                bool ti = terminators.find(i) == terminators.end();
                bool tj = terminators.find(j) == terminators.end();
                if (ti && tj) {
                    c.push_back(Et(i, j, -dval));
                    c.push_back(Et(j, i, -dval));
                }
                if (ti) {
                    d[i] += dval;
                    b(i) -= qval;
                }
                if (tj) {
                    d[j] += dval;
                    b(j) += qval;
                }
            }

            // Write to diagonal
            for (int i = 0; i < (int)d.size(); i++)
                c.push_back(Et(i, i, d[i] == 0.0 ? 1.0 : d[i]));
                    
            // Create matrix
            A.setFromTriplets(c.begin(), c.end());
            A.makeCompressed();

            // Solve matrix
            //Eigen::ConjugateGradient<Eigen::SparseMatrix<double> > solver(A); // Slightly slower but more robust
            Eigen::SparseLU<Eigen::SparseMatrix<double> > solver(A); // Direct solve
            solver.compute(A);

            if (solver.info() != Eigen::Success) {
                
                // Solver failed
                throw std::runtime_error("Flow network solver failed");
            }
            dh = solver.solve(b);

            // Update h
            h += dh;

            // Check convergence
            dh_dot = dh.dot(dh);
            if (iter == 0) {
                dh0_dot = dh_dot;

                // Check if any flow is present
                if (dh0_dot == 0.0) {
                    break;
                }
            }
            iter++;

            //std::cout << "Flow network convergence: " << dh_dot/dh0_dot << std::endl; // TEST

        } while(dh_dot/dh0_dot > 1.0E-3 && iter < 100);
                
        //std::cout << "Flow network took " << iter << " iterations" << std::endl; // TEST
        if (dh0_dot == 0.0) {
            throw std::runtime_error("Flow network has no sources");
        }
        if (iter == 100) {
            throw std::runtime_error("Flow network has not converged");
        }
        
        // Store pressure
        for (std::size_t k = 0; k < N; k++)
            network.template setProperty<TYPE>(pointIndexes[k], "head", h(k));

        // Reset flow in terminators
        for (auto p : terminators) {
            flowVector[p] = (TYPE)0.0;
        }
        
        // Store flow
        double outflow = 0.0;
        double Qmax = 0.0;
        for (std::size_t k = 0; k < lineStringIndexes.size(); k++) {
                
            // Get link index
            auto &l = lineStringIndexes[k];

            // Get indexes
            cl_uint i = linkPoints[k].first;
            cl_uint j = linkPoints[k].second;
            
            // Get link parameters
            NetworkSegmentType::Type type = 
                static_cast<NetworkSegmentType::Type>(network.template getProperty<int>(l, "type"));
            double lK = (double)network.template getProperty<TYPE>(l, "K");
            double lk = (double)network.template getProperty<TYPE>(l, "k");

            // Get h values from link
            double hi = h(i);
            double hj = h(j);

            // Calculate flow   
            double Q = 0.0;
            switch (type) {
                            
                case NetworkSegmentType::HazenWilliams: {
                        
                    if (hi > hj)
                        Q = pow((hi-hj)*lK, lk);
                    else
                        Q = -pow((hj-hi)*lK, lk);

                    } break;
                            
                case NetworkSegmentType::ManningOpenChannel: {
                        
                    if (hi > hj)
                        Q = pow((hi-hj)*lK, lk);
                    else
                        Q = -pow((hj-hi)*lK, lk);

                    } break;
                    
                case NetworkSegmentType::Logarithmic: {                                

                    Q = lK*log(lk*fabs(hi-hj)+1.0);
                    if (hi < hj)
                        Q = -Q;

                    } break;    

                case NetworkSegmentType::SqrtExp: {                                
                    
                    if (hi > hj)
                        Q = sqrt(lK*(hi-hj)+eps)*exp(-lk*lK*(hi-hj));
                    else
                        Q = -sqrt(lK*(hj-hi)+eps)*exp(-lk*lK*(hj-hi));

                    } break;

                default: {
                    std::stringstream err;
                    err << "Unknown segment type " << type;
                    throw std::runtime_error(err.str());
                }
            }

            // Find maximum Q value
            Qmax = std::max(Qmax, fabs(Q));

            // Update network
            flowVector[l] = (TYPE)Q;

            // Set flow at terminators
            if (terminators.find(i) != terminators.end()) {
                flowVector[pointIndexes[i]] += (TYPE)Q;
                outflow += Q;
            } 
            if (terminators.find(j) != terminators.end()) {
                flowVector[pointIndexes[j]] -= (TYPE)Q;
                outflow -= Q;
            }
        }

        // Check sum of flow, this should be relatively close to zero
        if (fabs(outflow+inflow)/Qmax > 1.0E-3)
            std::cout << "WARNING: Flow does not balance over network" << std::endl;

        return true;
    }

    // Float type definitions
    template bool NetworkFlowSolver<float>::init(Vector<float> &, std::string);
    template bool NetworkFlowSolver<float>::run();

    // Double type definitions
    template bool NetworkFlowSolver<double>::init(Vector<double> &, std::string);
    template bool NetworkFlowSolver<double>::run();
}
