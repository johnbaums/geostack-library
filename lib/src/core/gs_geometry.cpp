/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <algorithm>
#include <cmath>
#include <list>
#include <array>
#include <string>
#include <queue>
#include <functional>

#include "gs_geometry.h"
#include "gs_projection.h"

namespace Geostack
{
    // Maximum items in RTree nodes
    template <typename T> const int RTree<T>::maxItems = 16;

    /**
    * Geohash characters.
    */
    template <typename CTYPE>
    const std::string Coordinate<CTYPE>::geoHashEnc32 = "0123456789bcdefghjkmnpqrstuvwxyz";
    
    /**
    * Geohash bounding box for spherical coordinates.
    */
    template <typename CTYPE>
    const BoundingBox<CTYPE> BoundingBox<CTYPE>::geoHashBounds = { {-180.0, -90.0}, {180.0, 90.0} };

    /**
    * %Coordinate magnitude squared.
    * @return sum of squared components.
    */    
    template <typename T>
    T Coordinate<T>::magnitudeSquared() {
        return p*p+q*q+r*r+s*s;
    }

    /**
    * %Coordinate maximum.
    * Returns maximum of two %Coordinates
    * @param a left-hand Coordinate to evaluate.
    * @param b right-hand Coordinate to evaluate.
    * @return %Coordinate containing maximum of all components.
    */
    template <typename T>
    Coordinate<T> Coordinate<T>::max(const Coordinate<T> &a, const Coordinate<T> &b) {
        return Coordinate<T> {
            std::max(a.p, b.p),
            std::max(a.q, b.q),
            std::max(a.r, b.r),
            std::max(a.s, b.s)
        };
    }

    /**
    * %Coordinate minimum.
    * Returns minimum of two %Coordinates
    * @param a left-hand Coordinate to evaluate.
    * @param b right-hand Coordinate to evaluate.
    * @return %Coordinate containing minimum of all components.
    */
    template <typename T>
    Coordinate<T> Coordinate<T>::min(const Coordinate<T> &a, const Coordinate<T> &b) {
        return Coordinate<T> {
            std::min(a.p, b.p),
            std::min(a.q, b.q),
            std::min(a.r, b.r),
            std::min(a.s, b.s)
        };
    }
    
    /**
    * %Coordinate centroid.
    * Returns centroid of two %Coordinates
    * @param a left-hand Coordinate to evaluate.
    * @param b right-hand Coordinate to evaluate.
    * @return %Coordinate containing minimum of all components.
    */
    template <typename T>
    Coordinate<T> Coordinate<T>::centroid(const Coordinate<T> &a, const Coordinate<T> &b) {
        return Coordinate<T> {
            (T)0.5*(a.p+b.p),
            (T)0.5*(a.q+b.q),
            (T)0.5*(a.r+b.r),
            (T)0.5*(a.s+b.s)
        };
    }

    /**
    * Vertex GeoHash.
    * @return GeoHash string.
    */
    template <typename CTYPE>
    std::string Coordinate<CTYPE>::getGeoHash() {

        std::string r;

        // Generate quadtree index
        uint64_t index = BoundingBox<CTYPE>::geoHashBounds.createZIndex2D(*this);
        
        // Change to 60 bit value
        uint64_t v = index>>4;

        // Construct string from back
        for (unsigned i = 0; i < 12; i++) {
            r.insert(r.begin(), geoHashEnc32.at((int)(v&0x1F)));
            v >>= 5;
        }
        return r;
    }

    /**
    * %Coordinate output stream
    * @param os output stream.
    * @param c %Coordinate.
    * @return updated ostream.
    */
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const Coordinate<CTYPE> &c) {

        // Write data
        return os << 
            c.p << ", " <<
            c.q << ", " <<
            c.r << ", " <<
            c.s;
    }

    /**
    * %Coordinate comparison.
    * Returns true if coordinates are equal
    * @param a left-hand %Coordinate to compare.
    * @param b right-hand %Coordinate to compare.
    * @return true if coordinates are identical.
    */
    template <typename T>
    bool operator==(const Coordinate<T> &a, const Coordinate<T> &b) {
        return (
            a.p == b.p && 
            a.q == b.q && 
            a.r == b.r && 
            a.s == b.s);
    }

    /**
    * %Coordinate comparison.
    * Returns true if coordinates are not equal
    * @param a left-hand %Coordinate to compare.
    * @param b right-hand %Coordinate to compare.
    * @return true if coordinates are not identical.
    */
    template <typename T>
    bool operator!=(const Coordinate<T> &a, const Coordinate<T> &b) {
        return !(a == b);
    }

    /**
    * %Coordinate addition.
    * @param a left-hand Coordinate to evaluate.
    * @param b right-hand Coordinate to evaluate.
    * @return Resulting %Coordinate.
    */
    template <typename T>
    Coordinate<T> operator+(const Coordinate<T> &a, const Coordinate<T> &b) {
        return Coordinate<T> {
            a.p+b.p,
            a.q+b.q,
            a.r+b.r,
            a.s+b.s
        };
    }

    /**
    * %Coordinate subtraction.
    * @param a left-hand Coordinate to evaluate.
    * @param b right-hand Coordinate to evaluate.
    * @return Resulting %Coordinate.
    */
    template <typename T>
    Coordinate<T> operator-(const Coordinate<T> &a, const Coordinate<T> &b) {
        return Coordinate<T> {
            a.p-b.p,
            a.q-b.q,
            a.r-b.r,
            a.s-b.s
        };
    }

    /**
    * %BoundingBox constructor.
    * Initialises %BoundingBox to maximum possible extent.
    */
    template <typename T>
    BoundingBox<T>::BoundingBox() {
        reset();
    }

     /**
    * %BoundingBox copy constructor.
    */
    template <typename T>
    BoundingBox<T>::BoundingBox(const BoundingBox<T> &A):
        min(A.min),
        max(A.max) { }

    /**
    * %BoundingBox initialisation.
    * Initialises %BoundingBox from %Coordinate pair.
    */
    template <typename T>
    BoundingBox<T>::BoundingBox(Coordinate<T> a, Coordinate<T> b) {

        max = Coordinate<T>::max(a, b);
        min = Coordinate<T>::min(a, b);
    }
    
    /**
    * Extend %BoundingBox by scalar distance.
    * @param d distance to extend %BoundingBox
    */
    template <typename T>
    void BoundingBox<T>::extend(T d) {

        // Ensure maximum and minimum are retained after extension
        Coordinate<T> a = { min.p-d, min.q-d };
        Coordinate<T> b = { max.p+d, max.q+d };
        
        max = Coordinate<T>::max(a, b);
        min = Coordinate<T>::min(a, b);
    }
    
    /**
    * Extend %BoundingBox to fit %Coordinate.
    * @param A %Coordinate to include.
    */
    template <typename T>
    void BoundingBox<T>::extend(Coordinate<T> a) {
    
        max = Coordinate<T>::max(max, a);
        min = Coordinate<T>::min(min, a);
    }
    
    /**
    * Extend %BoundingBox to fit second %BoundingBox.
    * @param A %BoundingBox to include.
    */
    template <typename T>
    void BoundingBox<T>::extend(const BoundingBox<T> &A) {
    
        max = Coordinate<T>::max(max, A.max);
        min = Coordinate<T>::min(min, A.min);
    }
    
    /**
    * Create union of %BoundingBox with a second %BoundingBox.
    * @param A %BoundingBox for union.
    */
    template <typename T>
    BoundingBox<T> BoundingBox<T>::intersect(const BoundingBox<T> &A) {
    
        BoundingBox<T> I;

        // Find overlaps
        if (A.min.p >= min.p && A.min.p <= max.p) {
            I.min.p = A.min.p;
        } else if (min.p >= A.min.p && min.p <= A.max.p) {
            I.min.p = min.p;
        } else {
            return BoundingBox<T>();
        }

        if (A.max.p >= min.p && A.max.p <= max.p) {
            I.max.p = A.max.p;
        } else if (max.p >= A.min.p && max.p <= A.max.p) {
            I.max.p = max.p;
        } else {
            return BoundingBox<T>();
        }
        
        if (A.min.q >= min.q && A.min.q <= max.q) {
            I.min.q = A.min.q;
        } else if (min.q >= A.min.q && min.q <= A.max.q) {
            I.min.q = min.q;
        } else {
            return BoundingBox<T>();
        }

        if (A.max.q >= min.q && A.max.q <= max.q) {
            I.max.q = A.max.q;
        } else if (max.q >= A.min.q && max.q <= A.max.q) {
            I.max.q = max.q;
        } else {
            return BoundingBox<T>();
        }

        if (A.min.r >= min.r && A.min.r <= max.r) {
            I.min.r = A.min.r;
        } else if (min.r >= A.min.r && min.r <= A.max.r) {
            I.min.r = min.r;
        } else {
            return BoundingBox<T>();
        }

        if (A.max.r >= min.r && A.max.r <= max.r) {
            I.max.r = A.max.r;
        } else if (max.r >= A.min.r && max.r <= A.max.r) {
            I.max.r = max.r;
        } else {
            return BoundingBox<T>();
        }

        if (A.min.s >= min.s && A.min.s <= max.s) {
            I.min.s = A.min.s;
        } else if (min.s >= A.min.s && min.s <= A.max.s) {
            I.min.s = min.s;
        } else {
            return BoundingBox<T>();
        }

        if (A.max.s >= min.s && A.max.s <= max.s) {
            I.max.s = A.max.s;
        } else if (max.s >= A.min.s && max.s <= A.max.s) {
            I.max.s = max.s;
        } else {
            return BoundingBox<T>();
        }

        return I;
    }

    /**
    * Convert %BoundingBox to new projection.
    * @param to projection to convert to.
    * @param from current projection to convert from
    */
    template <typename T>
    BoundingBox<T> BoundingBox<T>::convert(ProjectionParameters<double> to, ProjectionParameters<double> from) {

        Coordinate<T> tc0 = { min.p, min.q };
        Coordinate<T> tc1 = { min.p, max.q };
        Coordinate<T> tc2 = { max.p, min.q };
        Coordinate<T> tc3 = { max.p, max.q };
        Projection::convert(tc0, to, from);
        Projection::convert(tc1, to, from);
        Projection::convert(tc2, to, from);
        Projection::convert(tc3, to, from);
        T txMin = std::min(std::min(tc0.p, tc1.p), std::min(tc2.p, tc3.p));
        T tyMin = std::min(std::min(tc0.q, tc1.q), std::min(tc2.q, tc3.q));
        T txMax = std::max(std::max(tc0.p, tc1.p), std::max(tc2.p, tc3.p));
        T tyMax = std::max(std::max(tc0.q, tc1.q), std::max(tc2.q, tc3.q));
        return BoundingBox<T>( { txMin, tyMin, min.r, min.s }, { txMax, tyMax, max.r, max.s });
    }
    
    /**
    * Reset %BoundingBox 
    * Expand the three spatial coordinates to maximum possible extent.
    */
    template <typename T>
    void BoundingBox<T>::reset() {
    
        min.p = std::numeric_limits<T>::max();
        min.q = std::numeric_limits<T>::max();
        min.r = std::numeric_limits<T>::max();
        min.s = std::numeric_limits<T>::max();

        max.p = std::numeric_limits<T>::lowest();
        max.q = std::numeric_limits<T>::lowest();
        max.r = std::numeric_limits<T>::lowest();
        max.s = std::numeric_limits<T>::lowest();
    }

    /**
    * Return 2D area of bounding box.
    * @return Area of bounding box.
    */
    template <typename T>
    T BoundingBox<T>::area2D() {

        // Check for null bounding box
        if (max.p < min.p || max.q < min.q) {
            return T();
        } else {
            return (max.p-min.p)*(max.q-min.q);
        }
    }

    /**
    * Return squared distance between bounding box centroids.
    * @param A %BoundingBox to measure distance between.
    * @return Squared distance between bounding box centroids.
    */
    template <typename T>
    T BoundingBox<T>::centroidDistanceSqr(const BoundingBox<T> &A) {

        Coordinate<T> d = Coordinate<T>::centroid(min, max)-Coordinate<T>::centroid(A.min, A.max);
        return d.magnitudeSquared();
    }

    /**
    * Return minimum squared distance between bounding boxes.
    * @param A %BoundingBox to measure distance between.
    * @return Minimum squared distance between bounding boxes.
    */
    template <typename T>
    T BoundingBox<T>::minimumDistanceSqr(const BoundingBox<T> &A) {

        // Handle to bounding box
        BoundingBox<T> &B = *this;

        // Create union of bounding boxes
        BoundingBox<T> U(A);
        U.extend(B);

        // Subtract bounding boxes from union
        auto extentA = A.extent();
        auto extentB = B.extent();
        auto extentU = U.extent();

        Coordinate<T> d = {
            std::max((T)0.0, extentU.p-extentA.p-extentB.p),
            std::max((T)0.0, extentU.q-extentA.q-extentB.q),
            std::max((T)0.0, extentU.r-extentA.r-extentB.r),
            std::max((T)0.0, extentU.s-extentA.s-extentB.s) };
            
        return d.magnitudeSquared();
    }

    /**
    * Return centroid of bounding box
    * @return Centroid of bounding box
    */
    template <typename T>
    Coordinate<T> BoundingBox<T>::centroid() const {
        return Coordinate<T>::centroid(min, max);
    }

    /**
    * Return extent of bounding box
    * @return Extent of bounding box
    */
    template <typename T>
    Coordinate<T> BoundingBox<T>::extent() const {
        return Coordinate<T> { 
            std::fmax(max.p-min.p, (T)0.0), 
            std::fmax(max.q-min.q, (T)0.0),
            std::fmax(max.r-min.r, (T)0.0),
            std::fmax(max.s-min.s, (T)0.0) };
    }

    /**
    * Create 64-bit Z index from 2D coordinate within %BoundingBox.
    * Converts the coordinate to unsigned 32-bit integers
    * and interleaves the bits, creating a quadtree index.
    * Adapted from: https://graphics.stanford.edu/~seander/bithacks.html.
    * @return quadtree index.
    */
    template <typename T>
    uint64_t BoundingBox<T>::createZIndex2D(Coordinate<T> c) const {
        
        double p = (c.p-min.p)/(max.p-min.p+std::numeric_limits<T>::min());
        uint64_t pIndex = (uint64_t)(std::fmax(0.0, std::fmin(p, 1.0))*4294967295.0);
        pIndex = (pIndex | (pIndex << 16)) & (uint64_t)0x0000FFFF0000FFFF;
        pIndex = (pIndex | (pIndex << 8 )) & (uint64_t)0x00FF00FF00FF00FF;
        pIndex = (pIndex | (pIndex << 4 )) & (uint64_t)0x0F0F0F0F0F0F0F0F;
        pIndex = (pIndex | (pIndex << 2 )) & (uint64_t)0x3333333333333333;
        pIndex = (pIndex | (pIndex << 1 )) & (uint64_t)0x5555555555555555;
        
        double q = (c.q-min.q)/(max.q-min.q+std::numeric_limits<T>::min());
        uint64_t qIndex = (uint64_t)(std::fmax(0.0, std::fmin(q, 1.0))*4294967295.0);
        qIndex = (qIndex | (qIndex << 16)) & (uint64_t)0x0000FFFF0000FFFF;
        qIndex = (qIndex | (qIndex << 8 )) & (uint64_t)0x00FF00FF00FF00FF;
        qIndex = (qIndex | (qIndex << 4 )) & (uint64_t)0x0F0F0F0F0F0F0F0F;
        qIndex = (qIndex | (qIndex << 2 )) & (uint64_t)0x3333333333333333;
        qIndex = (qIndex | (qIndex << 1 )) & (uint64_t)0x5555555555555555;

        return (pIndex << 1) | qIndex;
    }

    /**
    * Create 64-bit Z index from 4D coordinate within %BoundingBox.
    * Converts the coordinate to unsigned 16-bit integers
    * and interleaves the bits, creating a quadtree index.
    * Adapted from: https://graphics.stanford.edu/~seander/bithacks.html.
    * @return quadtree index.
    */
    template <typename T>
    uint64_t BoundingBox<T>::createZIndex4D(Coordinate<T> c) const {
        
        double p = (c.p-min.p)/(max.p-min.p+std::numeric_limits<T>::min());
        uint64_t pIndex = (uint64_t)(std::fmax(0.0, std::fmin(p, 1.0))*65535.0);
        pIndex = (pIndex | (pIndex << 24 )) & (uint64_t)0x000000FF000000FF;
        pIndex = (pIndex | (pIndex << 12 )) & (uint64_t)0x000F000F000F000F;
        pIndex = (pIndex | (pIndex << 6 )) & (uint64_t)0x0303030303030303;
        pIndex = (pIndex | (pIndex << 3 )) & (uint64_t)0x1111111111111111;
        
        double q = (c.q-min.q)/(max.q-min.q+std::numeric_limits<T>::min());
        uint64_t qIndex = (uint64_t)(std::fmax(0.0, std::fmin(q, 1.0))*65535.0);
        qIndex = (qIndex | (qIndex << 24 )) & (uint64_t)0x000000FF000000FF;
        qIndex = (qIndex | (qIndex << 12 )) & (uint64_t)0x000F000F000F000F;
        qIndex = (qIndex | (qIndex << 6 )) & (uint64_t)0x0303030303030303;
        qIndex = (qIndex | (qIndex << 3 )) & (uint64_t)0x1111111111111111;
        
        double r = (c.q-min.q)/(max.q-min.q+std::numeric_limits<T>::min());
        uint64_t rIndex = (uint64_t)(std::fmax(0.0, std::fmin(r, 1.0))*65535.0);
        rIndex = (rIndex | (rIndex << 24 )) & (uint64_t)0x000000FF000000FF;
        rIndex = (rIndex | (rIndex << 12 )) & (uint64_t)0x000F000F000F000F;
        rIndex = (rIndex | (rIndex << 6 )) & (uint64_t)0x0303030303030303;
        rIndex = (rIndex | (rIndex << 3 )) & (uint64_t)0x1111111111111111;
        
        double s = (c.q-min.q)/(max.q-min.q+std::numeric_limits<T>::min());
        uint64_t sIndex = (uint64_t)(std::fmax(0.0, std::fmin(s, 1.0))*65535.0);
        sIndex = (sIndex | (sIndex << 24 )) & (uint64_t)0x000000FF000000FF;
        sIndex = (sIndex | (sIndex << 12 )) & (uint64_t)0x000F000F000F000F;
        sIndex = (sIndex | (sIndex << 6 )) & (uint64_t)0x0303030303030303;
        sIndex = (sIndex | (sIndex << 3 )) & (uint64_t)0x1111111111111111;

        return (pIndex << 3) | (qIndex << 2) | (rIndex << 1) | sIndex;
    }
    
    /**
    * Create a quadrant index from coordinate within %BoundingBox.
    * This is a bitwise index indicating whether the quadrant contains the
    * coordinate or is empty.
    * @return quadrant fill index.
    */
    template <typename T>
    uint64_t BoundingBox<T>::quadrant(Coordinate<T> c) const {

        uint64_t index = 0;

        index |= (2.0*c.p > max.p+min.p) ? (uint64_t)1 : (uint64_t)0;
        index |= (2.0*c.q > max.q+min.q) ? (uint64_t)2 : (uint64_t)0;
        index |= (2.0*c.r > max.r+min.r) ? (uint64_t)4 : (uint64_t)0;
        index |= (2.0*c.s > max.s+min.s) ? (uint64_t)8 : (uint64_t)0;

        return index;
    }

    /**
    * Check if coordinate is within bounding box
    * @param A bounding box.
    * @param c coordinate.
    * @return true if coordinate c is within bounding box A
    */
    template <typename T>
    bool BoundingBox<T>::boundingBoxContains(const BoundingBox<T> A, const Coordinate<T> c) {
        return (
            c.p >= A.min.p && c.p <= A.max.p && 
            c.q >= A.min.q && c.q <= A.max.q && 
            c.r >= A.min.r && c.r <= A.max.r && 
            c.s >= A.min.s && c.s <= A.max.s);
    }
    
    /**
    * Check if bounding box is within bounding box
    * @param A bounding box.
    * @param B bounding box.
    * @return true if bounding box B is within bounding box A
    */
    template <typename T>
    bool BoundingBox<T>::boundingBoxContains(const BoundingBox<T> A, const BoundingBox<T> B) {
        return (
            A.min.p <= B.min.p && A.max.p >= B.max.p && 
            A.min.q <= B.min.q && A.max.q >= B.max.q && 
            A.min.r <= B.min.r && A.max.r >= B.max.r && 
            A.min.s <= B.min.s && A.max.s >= B.max.s);
    }
    
    /**
    * Check if bounding box intersects with bounding box
    * @param A bounding box.
    * @param B bounding box.
    * @return true if bounding box A intersects with bounding box B
    */
    template <typename T>
    bool BoundingBox<T>::boundingBoxIntersects(const BoundingBox<T> A, const BoundingBox<T> B) {
        return (
            A.min.p <= B.max.p && A.max.p >= B.min.p && 
            A.max.q >= B.min.q && A.min.q <= B.max.q && 
            A.max.r >= B.min.r && A.min.r <= B.max.r && 
            A.max.s >= B.min.s && A.min.s <= B.max.s);
    }

    /**
    * %Box construction.
    * Creates %Box from pair of coordinates
    */
    template <typename T>
    Box<T>::Box() { }
    
    /**
    * %Box construction.
    * Creates %Box from pair of coordinates
    */
    template <typename T>
    Box<T>::Box(BoundingBox<T> bounds_):
        bounds(bounds_) { }

    /**
    * Get %Box bounding box.
    * @return pair of coordinates spanning the bounding box of the box.
    */
    template <typename T>
    BoundingBox<T> Box<T>::getBounds() const {
        return bounds;
    }

    /**
    * %BoundingBox comparison.
    * Returns true if coordinates are equal
    * @param a left-hand %BoundingBox to compare.
    * @param b right-hand %BoundingBox to compare.
    * @return true if bounding boxes are identical.
    */
    template <typename T>
    bool operator==(const BoundingBox<T> &a, const BoundingBox<T> &b) {
        return (a.min == b.min && a.max == b.max);
    }

    /**
    * %BoundingBox comparison.
    * Returns true if coordinates are not equal
    * @param a left-hand %BoundingBox to compare.
    * @param b right-hand %BoundingBox to compare.
    * @return true if bounding boxes are not identical.
    */
    template <typename T>
    bool operator!=(const BoundingBox<T> &a, const BoundingBox<T> &b) {
        return !(a == b);
    }

    /**
    * Adjust internal bounds to fit given bounds.
    * @param input b bounds.
    */
    template <typename T>
    void RTreeNode<T>::fitBounds(const BoundingBox<T> &B) {

        // Fit box to input bounds B
        bounds.extend(B);
    }
    
    /**
    * Adjust internal bounds to items in nodes.
    */
    template <typename T>
    void RTreeNode<T>::fitBoundsToNodes() {
    
        // Reset bounds
        bounds.reset();

        // Adjust to nodes
        for (auto &n : nodes)
            bounds.extend(n->getBounds());
    }

    /**
    * %RTree contructor.
    * Creates root node.
    */
    template <typename T>
    RTree<T>::RTree() {

        // Create root
        root = std::make_shared<RTreeNode<T> >();

        // Reserve entries
        root->nodes.reserve(maxItems+1);
    }

    /**
    * Clear tree
    */
    template <typename T>
    void RTree<T>::clear() {

        // Create new root
        root = std::make_shared<RTreeNode<T> >();

        // Reserve entries
        root->nodes.reserve(maxItems+1);
    }

    /**
    * Insert geometry into %RTree.
    * @gty @Geometry to add.
    */
    template <typename T>
    void RTree<T>::insert(GeometryBasePtr<T> gty) {
    
        // Get bounds
        auto bounds = gty->getBounds();

        // Create ancestor queue
        std::list<RTreeNodePtr<T> > nodeAncestors;
        nodeAncestors.push_front(nullptr);

        // Set initial node to root
        RTreeNodePtr<T> node = root;
        node->fitBounds(bounds);

        // Find leaf node by moving through all nodes with child containers
        while (node->nodes.size() > 0 && node->nodes[0]->isContainer()) {

            // Add to ancestors
            nodeAncestors.push_front(node);

            // Find best node to expand to hold geometry
            int minIndex = 0;
            T minDistanceSqr = std::numeric_limits<T>::max();
            for (int index = 0; index < node->nodes.size(); index++) {

                // Find centroid distance
                auto nodeBounds = node->nodes[index]->getBounds();
                T distanceSqr = nodeBounds.centroidDistanceSqr(bounds);

                // Check and update index
                if (distanceSqr < minDistanceSqr) {
                    minDistanceSqr = distanceSqr;
                    minIndex = index;
                }
            }

            // Update node
            node = std::static_pointer_cast<RTreeNode<T> >(node->nodes[minIndex]);

            // Fit node to geometry bounds
            node->fitBounds(bounds);
        }

        // Insert geometry
        for (auto &parent : nodeAncestors) {
        
            // Add node
            node->nodes.push_back(gty);

            // Check for split
            if (node->nodes.size() < maxItems) {

                // Exit loop if node has enough room
                break;

            } else {

                // Create split node
                auto split = std::make_shared<RTreeNode<T> >();
            
                // Create quadrant counts
                std::multimap<uint64_t, GeometryBasePtr<T> > nodeIndex;
                bounds = node->getBounds();
                for (auto &n : node->nodes) {

                    // Calculate quadrant
                    uint64_t Z = bounds.createZIndex4D(n->getBounds().centroid());

                    // Update lists and count
                    nodeIndex.insert( { Z, n } );
                }
                        
                // Clear nodes
                node->nodes.clear();

                // Reserve space in nodes
                node->nodes.reserve(maxItems+1);
                split->nodes.reserve(maxItems+1);

                // Distribute nodes
                size_t count = 0;
                for (auto it = nodeIndex.begin(); it != nodeIndex.end(); it++, count++) {
                    if (count < (maxItems>>1)) {
                        node->nodes.push_back(it->second);
                    } else {
                        split->nodes.push_back(it->second);
                    }
                }

                // Update bounds
                node->fitBoundsToNodes();
                split->fitBoundsToNodes();

                // Add split node to tree
                if (parent == nullptr) {

                    // Create new root if parent is null
                    parent = std::make_shared<RTreeNode<T> >();

                    // Add nodes
                    parent->nodes.push_back(node);
                    parent->nodes.push_back(split);

                    // Resize and set root                    
                    parent->fitBoundsToNodes();
                    root = parent;

                } else {

                    // Update geometry
                    gty = std::static_pointer_cast<GeometryBase<T> >(split);
                    node = parent;
                }
            }
        }
    }

    /**
    * Recursive function for locating bounding box parent in %RTree.
    * @param bounds %BoundingBox of search.
    * @param searchGeometry list of geometry.
    * @param node current %RTreeNode.
    */
    template <typename T>
    void RTree<T>::search(BoundingBox<T> bounds, std::vector<GeometryBasePtr<T> > &searchGeometry, 
        size_t types, RTreeNodePtr<T> node) {
    
        // Set node to root if no node is supplied
        if (node == nullptr) {
            searchGeometry.clear();
            node = root;
        }

        // Recursively search containers
        for (auto &childNode : node->nodes) {
        
            if (BoundingBox<T>::boundingBoxIntersects(bounds, childNode->getBounds())) {
                if (childNode->isContainer()) {

                    // Recursively search containers
                    search(bounds, searchGeometry, types, std::static_pointer_cast<RTreeNode<T> >(childNode));

                } else {

                    // TODO element intersection

                    // Add geometry to vector
                    if (childNode->isType(types))
                        searchGeometry.push_back(childNode);
                }
            }
        }
    }
    
    /**
    * Search and return nearest geometry to %BoundingBox.
    * The search returns all geometry that could be the closest to any point within the bounding box.
    * @param bounds %BoundingBox of search.
    * @param searchGeometry list of geometry.
    * @param node current %RTreeNode.
    */
    template <typename T>
    void RTree<T>::nearest(BoundingBox<T> bounds, std::vector<GeometryBasePtr<T> > &nearestGeometry, size_t types) {

        // Check for data in RTree
        if (root->nodes.size() == 0)
            return;

        // Create queue
        typedef std::pair<T, GeometryBasePtr<T> > geometryPair; 
        std::priority_queue<geometryPair, std::vector<geometryPair>, std::greater<geometryPair> > q;
        
        // Add root element
        q.push( { (T)0.0, root } );
        RTreeNodePtr<T> topNode = root;

        do {

            // Remove top of queue
            q.pop();

            // Add all nodes to queue
            for (auto &childNode : topNode->nodes)
                q.push( { bounds.minimumDistanceSqr(childNode->getBounds()), childNode } );

            // Get top of queue
            topNode = std::static_pointer_cast<RTreeNode<T> >(q.top().second);

        } while(topNode->isContainer() && !topNode->isType(types));

        // Get shortest distance to geometry
        T searchDistance = sqrt(q.top().first);

        // Extend bounds to encompass all possible nearest geometry
        auto bExtent = bounds.extent();
        searchDistance += hypot(bExtent.p, bExtent.q);
        bounds.max.p += searchDistance;
        bounds.min.p -= searchDistance;
        bounds.max.q += searchDistance;
        bounds.min.q -= searchDistance;

        // Search for geometry within region
        search(bounds, nearestGeometry, types);
    }

    // Float type definitions
    template class Coordinate<float>;
    template class BoundingBox<float>;
    template class GeometryBase<float>;
    template class Box<float>;
    template class RTreeNode<float>;
    template class RTree<float>;
    
    template bool operator==(const Coordinate<float> &, const Coordinate<float> &);
    template bool operator!=(const Coordinate<float> &, const Coordinate<float> &);
    template Coordinate<float> operator+(const Coordinate<float> &, const Coordinate<float> &);
    template Coordinate<float> operator-(const Coordinate<float> &, const Coordinate<float> &);
    template std::ostream &operator<<(std::ostream &, const Coordinate<float> &);

    template bool operator==(const BoundingBox<float> &, const BoundingBox<float> &);
    template bool operator!=(const BoundingBox<float> &, const BoundingBox<float> &);
    
    // Double type definitions
    template class Coordinate<double>;
    template class BoundingBox<double>;
    template class GeometryBase<double>;
    template class Box<double>;
    template class RTreeNode<double>;
    template class RTree<double>;
    
    template bool operator==(const Coordinate<double> &, const Coordinate<double> &);
    template bool operator!=(const Coordinate<double> &, const Coordinate<double> &);
    template Coordinate<double> operator+(const Coordinate<double> &, const Coordinate<double> &);
    template Coordinate<double> operator-(const Coordinate<double> &, const Coordinate<double> &);
    template std::ostream &operator<<(std::ostream &, const Coordinate<double> &);

    template bool operator==(const BoundingBox<double> &, const BoundingBox<double> &);
    template bool operator!=(const BoundingBox<double> &, const BoundingBox<double> &);
}
