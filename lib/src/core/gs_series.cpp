/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

 #include <algorithm>
#include <sstream>
 
 #include "gs_solver.h"
 #include "gs_string.h"
 #include "gs_series.h"
 
namespace Geostack
{
    /**
     * Series constructor
     */
    template <typename XTYPE, typename YTYPE>
    Series<XTYPE, YTYPE>::Series(): 
        name(), 
        interpolation(SeriesInterpolation::Linear) {

        // Reset values
        clear();
    }

    /**
     * Series copy constructor
     * @param r %Series to copy.
     */
    template <typename XTYPE, typename YTYPE>
    Series<XTYPE, YTYPE>::Series(const Series<XTYPE, YTYPE> &r):
        name(r.name), 
        interpolation(r.interpolation), 
        values(r.values), 
        slopes(r.slopes),
        x_max(r.x_max), 
        x_min(r.x_min), 
        y_max(r.y_max), 
        y_min(r.y_min),
        y_upperBound(r.y_upperBound), 
        y_lowerBound(r.y_lowerBound) { }

    /**
     * Series assignment operator
     * @param r %Series to copy.
     */
    template <typename XTYPE, typename YTYPE>
    Series<XTYPE, YTYPE> &Series<XTYPE, YTYPE>::operator=(const Series<XTYPE, YTYPE> &r) {

        if (this != &r) {
        
            // Copy data
            name = r.name;
            interpolation = r.interpolation;
            values = r.values;
            slopes = r.slopes;

            x_max = r.x_max;
            x_min = r.x_min;
            y_max = r.y_max; 
            y_min = r.y_min;

            y_upperBound = r.y_upperBound; 
            y_lowerBound = r.y_lowerBound;
        }
        return *this;
    }

    /**
     * Clear series
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::clear() {

        // Clear all vectors
        values.clear();
        slopes.clear();

        // Update limits
        x_max = getNullValue<XTYPE>();
        x_min = getNullValue<XTYPE>();
        y_max = getNullValue<YTYPE>();
        y_min = getNullValue<YTYPE>();

        // Update bounds
        y_upperBound = getNullValue<YTYPE>();
        y_lowerBound = getNullValue<YTYPE>();
    }

    /**
     * Adds a value to a series and re-calculates gradients.
     * @param y ordinate value
     * @param x abscissa value
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::addValue(XTYPE x, YTYPE y) {

        values.push_back( { x, y } );
        update();
    }

    /**
     * Adds a value to a series and re-calculates gradients.
     * @param y ordinate value
     * @param x string abscissa value
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::addValue(std::string x, YTYPE y) {
    
        // Try to convert from numeric string
        if (Strings::isNumber(x)) {
            values.push_back( { Strings::toNumber<XTYPE>(x), y } );
        } else {

            // Try to convert from ISO8601 date
            auto timeSinceEpoch = Strings::iso8601toEpoch(x);
            if (timeSinceEpoch != getNullValue<int64_t>() ) {
                values.push_back( { (XTYPE)timeSinceEpoch, y } );
            } else {
                std::stringstream err;
                err << "Invalid ISO8601 string (time zone must be included) '" << x << "'";
                throw std::runtime_error(err.str());
            }
        }
        update();
    }

    /**
     * Adds a vector of values to a series and re-calculates gradients.
     * @param newValues vector of %SeriesPair values
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::addValues(const std::vector<SeriesItem<XTYPE, YTYPE> > newValues) {

        values.insert(values.end(), newValues.begin(), newValues.end());
        update();
    }

    /**
     * Adds a vector of string and value pairs to a series and re-calculates gradients.
     * @param newValues vector of %SeriesPair values
     */
    template <typename XTYPE, typename YTYPE>
    bool Series<XTYPE, YTYPE>::addValues(const std::vector<std::pair<std::string, YTYPE> > newValues) {

        // Parse values
        for (auto &value : newValues) {

            // Try to convert from numeric string
            if (Strings::isNumber(value.first)) {
                values.push_back( { Strings::toNumber<XTYPE>(value.first), value.second } );
            } else {

                // Try to convert from ISO8601 date
                auto timeSinceEpoch = Strings::iso8601toEpoch(value.first);
                if (timeSinceEpoch == getNullValue<int64_t>() ) {
                    return false;
                } else {
                    values.push_back( { (XTYPE)timeSinceEpoch, value.second } );
                }
            }
        }

        // Update series
        update();
        return true;
    }

    /**
     * Calculate gradients for monotone cubic Hermite interpolation. Derivatives are limited using Fritsch-Carlson method.
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::update() {

        // Sort values
        std::sort(values.begin(), values.end(), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });

        // Update limits
        updateLimits();

        // Calculate the tangents for each point
        std::size_t n = values.size();
        if (n == 0)
            return;
        slopes.resize(n);

        // Special case for n = 1
        if (n == 1) {
            slopes[0] = 0.0;
            return;
        }

        // Calculate derivatives
        std::vector<double> d(n-1);
        std::vector<double> h(n-1);
        for (int i = 0; i < n-1; i++) {
            h[i] = (double)(values[i+1].x-values[i].x);
            d[i] = (double)(values[i+1].y-values[i].y)/h[i];
        }

        // Calculate tangents
        slopes[0] = d[0];
        slopes[n-1] = d[n-2];
        for (std::size_t i = 1; i < n-1; i++)
            slopes[i] = 0.5*(d[i-1]+d[i]);

        // Limit derivatives using Fritsch-Carlson method
        // See: http://math.stackexchange.com/questions/45218/implementation-of-monotone-cubic-interpolation
        // and: http://dx.doi.org/10.1137/0717021            
        for (std::size_t i = 1; i < n-1; i++) {

            // Compare derivative signs
            if (d[i]*d[i-1] < 0.0) {
                slopes[i] = 0.0;
            } else {
                slopes[i] = 3.0*(h[i-1]+h[i])/(((2.0*h[i]+h[i-1])/d[i-1])+((h[i]+2.0*h[i-1])/d[i]));
            }
        }
        
    }

    /**
     * Find maximum and minimum values in series.
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::updateLimits() {
    
        // x-values are sorted
        x_min = values[0].x;
        x_max = values[values.size()-1].x;

        // y-values are unsorted
        y_min = values[0].y;
        y_max = values[0].y;
        for (int i = 1; i < values.size(); i++) {
            YTYPE &y = values[i].y;
            if (y < y_min) y_min = y;
            if (y > y_max) y_max = y;
        }
    }
        
    /**
     * Find if a value is within the range of a series.
     * @param x abscissa value to check.
     * @return \b true if within range, \b false otherwise.
     */
    template <typename XTYPE, typename YTYPE>
    bool Series<XTYPE, YTYPE>::inRange(const XTYPE x) {
    
        // Return false for null series
        if (values.size() == 0)
            return false;

        // Return true for constant series
        if (values.size() == 1)
            return true;

        // Check bounds
        if (x < x_min || x > x_max)
            return false;
        
        return true;
    }

    /**
    * Set bounds for bounded interpolation
    */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::setBounds(YTYPE y_firstBound, YTYPE y_secondBound) { 
        y_upperBound = std::max(y_firstBound, y_secondBound);
        y_lowerBound = std::min(y_firstBound, y_secondBound);
    }

    /**
     * Linear series interpolation.
     * @param x abscissa value.
     * @return linear interpolation of series at point <b>x</b>.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::getLinear(XTYPE x) {

        // Check range
        if (!inRange(x))
            return getNullValue<YTYPE>();
        
        // Find lower index around x
        auto ilo = std::lower_bound(values.begin(), values.end(), SeriesItem<XTYPE, YTYPE>( { x, (YTYPE)0 } ), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });
        std::size_t lo = ilo-values.begin();
        if (lo > 0) lo--;

        // Linear interpolation
        double iVal = (double)(values[lo+1].y-values[lo].y)*(double)(x-values[lo].x)/(double)(values[lo+1].x-values[lo].x);
        return values[lo].y+(YTYPE)iVal;
    }

    /**
     * Monotone cubic series interpolation.
     * @param x abscissa value.
     * @return monotone cubic interpolation of series at point <b>x</b>.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::getMonotoneCubic(XTYPE x) {
    
        // Check range
        if (!inRange(x))
            return getNullValue<YTYPE>();
        
        // Find lower index around x
        auto ilo = std::lower_bound(values.begin(), values.end(), SeriesItem<XTYPE, YTYPE>( { x, (YTYPE)0 } ), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });
        std::size_t lo = ilo-values.begin();
        if (lo > 0) lo--;

        // Cubic spline interpolation
        // See: http://math.stackexchange.com/questions/4082/equation-of-a-curve-given-3-points-and-additional-constant-requirements#4104
        double idx = 1.0/(double)(values[lo+1].x-values[lo].x);
        double s = (double)(values[lo+1].y-values[lo].y)*idx;
        double c = (3.0*s-2.0*slopes[lo]-slopes[lo+1])*idx;
        double d = (slopes[lo]+slopes[lo+1]-2.0*s)*(idx*idx);

        // Cubic interpolation
        double dx = (double)(x-values[lo].x);
        double iVal = (slopes[lo]+(c+d*dx)*dx)*dx;
        return values[lo].y+(YTYPE)iVal;
    }

    /**
     * Bounded linear series interpolation.
     * @param x abscissa value.
     * @return linear interpolation of series at point <b>x</b> bounded by upper and lower bounds.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::getBoundedLinear(XTYPE x) {
    
        // Check range
        if (!inRange(x))
            return getNullValue<YTYPE>();
        
        // Find lower index around x
        auto ilo = std::lower_bound(values.begin(), values.end(), SeriesItem<XTYPE, YTYPE>( { x, (YTYPE)0 } ), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });
        std::size_t lo = ilo-values.begin();
        if (lo > 0) lo--;

        // Bounded linear interpolation
        YTYPE y_0 = values[lo].y;
        YTYPE y_1 = values[lo+1].y;

        double delta_y = 0.0;
        if (y_0 > y_1) {

            double step_A = (double)(y_0-y_1);
            double step_B = (double)(y_1-y_lowerBound)+(double)(y_upperBound-y_0);
            delta_y = step_A < step_B ? -step_A : step_B;
        } else if (y_0 < y_1) {

            double step_A = (double)(y_1-y_0);
            double step_B = (double)(y_0-y_lowerBound)+(double)(y_upperBound-y_1);
            delta_y = step_A < step_B ? step_A : -step_B;
        }

        double delta_x = (double)(values[lo+1].x-values[lo].x);
        double iVal = (double)(x-values[lo].x)*delta_y/delta_x;
        YTYPE rVal = y_0+(YTYPE)iVal;

        if (rVal < y_lowerBound) 
            rVal+=(y_upperBound-y_lowerBound);
        else if (rVal > y_upperBound) 
            rVal-=(y_upperBound-y_lowerBound);

        return rVal;
    }

    /**
     * Series interpolation.
     * @param x abscissa value.
     * @return constant value for constant series or monotone cubic interpolation of series at point <b>x</b>.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::operator()(const XTYPE x) {
    
        // Return first value for constant series
        if (values.size() == 1)
            return values[0].y;

        // Return interpolated value
        switch (interpolation) {
            case SeriesInterpolation::Linear:
                return getLinear(x);

            case SeriesInterpolation::MonotoneCubic:
                return getMonotoneCubic(x);  

            case SeriesInterpolation::BoundedLinear:
                return getBoundedLinear(x);  
        }

        // Return no-data value
        return getNullValue<YTYPE>();
    }

    // Definitions
    template class Series<double, float>;
    template class Series<int64_t, float>;
    template class Series<double, double>;
    template class Series<int64_t, double>;
}