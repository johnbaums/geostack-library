/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>

#include "miniz.h"

#include "gs_string.h"
#include "gs_geowkt.h"

namespace Geostack
{
    /**
    * GeoWKT parser.
    * Converts a GeoWKT string to a %Vector object
    * @param geoWKTStr GeoWKT string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> GeoWKT<T>::geoWKTFileToVector(std::string geoWKTFile) {

        // Create return Vector
        Vector<T> v;

        std::ifstream f(geoWKTFile, std::ios::in | std::ios::binary);
        if (f) {

            // Read first character
            char c;
            f.get(c);

            std::locale loc;
            if (std::isalpha(c, loc)) {

                // Read file into string
                std::string geoWKTStr;
                f.seekg(0, std::ios::end);
                geoWKTStr.resize(f.tellg());
                f.seekg(0, std::ios::beg);
                f.read(&geoWKTStr[0], geoWKTStr.size());
                f.close();

                // Parse string
                parseString(v, geoWKTStr);

            } else {

                // Try and open as zip file
                mz_zip_archive zGeoWKT;
                std::memset(&zGeoWKT, 0, sizeof(zGeoWKT));
                if (mz_zip_reader_init_file(&zGeoWKT, geoWKTFile.c_str(), 0)) {

                    // Loop through files
                    std::size_t size;
                    for (mz_uint i = 0; i < (mz_uint)mz_zip_reader_get_num_files(&zGeoWKT); i++) {

                        mz_zip_archive_file_stat zStat;
                        if (mz_zip_reader_file_stat(&zGeoWKT, i, &zStat)) {

                            // Skip directory entries
                            if (!mz_zip_reader_is_file_a_directory(&zGeoWKT, i)) {

                                // Read data
                                void *data = mz_zip_reader_extract_file_to_heap(&zGeoWKT, zStat.m_filename, &size, 0);

                                // Copy to string
                                std::string geoWKTStr;
                                geoWKTStr.resize(size);
                                std::memcpy(&geoWKTStr[0], data, size);

                                // Free data
                                mz_free(data);

                                // Check data and uncompressed size
                                if (data && zStat.m_uncomp_size == size) {

                                    // Parse string
                                    parseString(v, geoWKTStr);

                                } else {
                                    throw std::runtime_error("Zip uncompression failed");
                                }
                            }
                        }
                    }
                }
            }
        } else {
            std::stringstream err;
            err << "Cannot open '" << geoWKTFile << "' for reading";
            throw std::runtime_error(err.str());
        }

        // Return Vector
        return v;
    }

    /**
    * GeoWKT parser.
    * Converts a GeoWKT string to a %Vector object
    * @param geoWKTStr GeoWKT string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> GeoWKT<T>::geoWKTToVector(std::string geoWKTStr) {

        // Create return Vector
        Vector<T> v;

        // Parse string
        parseString(v, geoWKTStr);

        // Return Vector
        return v;
    }

    /**
    * GeoWKT coordinate handler.
    * Parses GeoWKT coordinate.
    * @param geoWKTCoordStr coordinate string.
    * @return %Coordinate object.
    */
    template <typename T>
    Coordinate<T> GeoWKT<T>::parseCoordinate(std::string geoWKTCoordStr, std::locale &loc) {

        std::size_t ci = 0;
        Coordinate<T> c;
        std::size_t start = 0;
        std::size_t nChars = 0;

        for (std::size_t i = 0; i < geoWKTCoordStr.size(); i++) {

            // Skip characters
            while (i < geoWKTCoordStr.size() && std::isspace(geoWKTCoordStr[i], loc)) {
                i++;
            }

            // Reset counters
            start = i;
            nChars = 0;

            // Read value
            while (i < geoWKTCoordStr.size() && !std::isspace(geoWKTCoordStr[i], loc)) {
                nChars++;
                i++;
            }

            // Set value
            T val = Strings::toNumber<T>(geoWKTCoordStr.substr(start, nChars));
            if (ci == 0) {
                c.p = val;
            } else if (ci == 1) {
                c.q = val;
            } else if (ci == 2) {
                c.r = val;
            } else if (ci == 3) {
                c.s = val;
            }
            ci++;
        }

        return c;
    }

    /**
    * GeoWKT parser.
    * Converts a GeoWKT string to a %Vector object
    * @param geoWKTStr GeoWKT string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    std::vector<cl_uint> GeoWKT<T>::parseString(Geostack::Vector<T> &v, std::string geoWKTStr) {

        std::size_t start = 0;
        std::size_t nChars = 0;
        std::string token;
        std::locale loc;

        // Create return list
        std::vector<cl_uint> geometryIDs;

        for (std::size_t i = 0; i < geoWKTStr.size(); i++) {
            if (geoWKTStr[i] == '(') {

                // Get substring
                token = geoWKTStr.substr(start, nChars);

                // Skip characters
                i++;
                while (i < geoWKTStr.size() && std::isspace(geoWKTStr[i], loc)) {
                    i++;
                }

                // Parse known tokens
                if (token == "POINT") {

                    // Reset counters
                    start = i;
                    nChars = 0;

                    while (i < geoWKTStr.size() && geoWKTStr[i] != ')') {
                        nChars++;
                        i++;
                    }

                    // Add point
                    geometryIDs.push_back(v.addPoint(parseCoordinate(geoWKTStr.substr(start, nChars), loc)));

                    // Skip brace
                    i++;
                } else if (token == "LINESTRING") {

                    // Create coordinate list
                    CoordinateList<T> cs;

                    while (i < geoWKTStr.size() && geoWKTStr[i] != ')') {

                        // Reset counters
                        start = i;
                        nChars = 0;

                        while (i < geoWKTStr.size() && geoWKTStr[i] != ',' && geoWKTStr[i] != ')') {
                            nChars++;
                            i++;
                        }
                        if (geoWKTStr[i] == ',') {
                            i++;
                        }

                        // Add to coordinate list
                        cs.push_back(parseCoordinate(geoWKTStr.substr(start, nChars), loc));
                    }

                    // Add line string
                    geometryIDs.push_back(v.addLineString(cs));

                    // Skip brace
                    i++;
                } else if (token == "POLYGON") {

                    // Create coordinate lists
                    CoordinateList<T> cs;
                    std::vector<CoordinateList<T>> pcs;

                    while (i < geoWKTStr.size() && geoWKTStr[i] != ')') {

                        if (geoWKTStr[i] == '(') {

                            // Skip characters
                            i++;
                            while (i < geoWKTStr.size() && std::isspace(geoWKTStr[i], loc)) {
                                i++;
                            }

                            // Parse sub-polygons
                            while (i < geoWKTStr.size() && geoWKTStr[i] != ')') {

                                // Reset counters
                                start = i;
                                nChars = 0;

                                while (i < geoWKTStr.size() && geoWKTStr[i] != ',' && geoWKTStr[i] != ')') {
                                    nChars++;
                                    i++;
                                }
                                if (geoWKTStr[i] == ',') {
                                    i++;
                                }

                                // Add to coordinate list
                                cs.push_back(parseCoordinate(geoWKTStr.substr(start, nChars), loc));
                            }

                            // Add to polygon list
                            pcs.push_back(cs);
                            cs.clear();

                            // Skip characters
                            i++;
                            while (i < geoWKTStr.size() && (std::isspace(geoWKTStr[i], loc) || geoWKTStr[i] == ',')) {
                                i++;
                            }
                        }
                    }

                    // Add polygon
                    geometryIDs.push_back(v.addPolygon(pcs));

                    // Skip brace
                    i++;
                }

                // Skip characters
                while (i < geoWKTStr.size() && (std::isspace(geoWKTStr[i], loc) || geoWKTStr[i] == ',')) {
                    i++;
                }

                // Reset counters
                start = i;
                nChars = 0;
            }

            // Closing brace
            if (geoWKTStr[i] == ')') {
                break;
            }

            if (!std::isspace(geoWKTStr[i], loc)) {
                nChars++;
            }
        }

        return geometryIDs;
    }
    
    /**
    * GeoWKT parser.
    * Converts a GeoWKT string to a %Vector object
    * @param geoWKTStr GeoWKT string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    std::vector<cl_uint> GeoWKT<T>::parseStrings(Geostack::Vector<T> &v, std::vector<std::string> geoWKTStrs) {

        // Create return list
        std::vector<cl_uint> geometryIDs;

        // Parse strings
        for (auto &geoWKTStr: geoWKTStrs) {
            auto geometryStringIDs = GeoWKT<T>::parseString(v, geoWKTStr);
            geometryIDs.insert(geometryIDs.end(), geometryStringIDs.begin(), geometryStringIDs.end());
        }
        return geometryIDs;
    }

    /**
    * GeoWKT parser.
    * Converts a %Vector object to a GeoWKT string
    * @param v %Vector object.
    * @return GeoWKT string.
    */
    template <typename T>
    std::string GeoWKT<T>::vectorToGeoWKT(Geostack::Vector<T> &v) {
    
        // Get geometry
        auto &geometryIndexes = v.getGeometryIndexes();

        // Build WKT string
        std::string geoWKT;
        if (geometryIndexes.size() > 0) {

            if (geometryIndexes.size() > 1) {
                geoWKT += "GEOMETRYCOLLECTION (";
            }

            // Add geometry elements
            for (const auto &index : geometryIndexes) {
                auto &g = v.getGeometry(index);
                geoWKT += vectorItemToGeoWKT(v, index);
                geoWKT += ", ";
            }
            geoWKT.erase(geoWKT.length()-2);
        
            if (geometryIndexes.size() > 1) {
                geoWKT += ")";
            }
        }
        return geoWKT;
    }

    /**
    * GeoWKT parser.
    * Converts a %geometry object to a GeoWKT string
    * @param v %Vector object.
    * @param index %Geometry index
    * @return GeoWKT string.
    */
    template <typename T>
    std::string GeoWKT<T>::vectorItemToGeoWKT(Geostack::Vector<T> &v, std::size_t index) {

        std::string geoWKT;

        // Check type
        auto &g = v.getGeometry(index);

        // Process type
        if (g->isType(GeometryType::Point)) {

            // Add point
            auto c = v.getPointCoordinate(index);
            geoWKT += "POINT (" + std::to_string(c.p) + " " + std::to_string(c.q) + "), ";

        } else if (g->isType(GeometryType::LineString)) {

            // Add line string
            geoWKT += "LINESTRING (";
            for (auto c : v.getLineStringCoordinates(index)) {

                // Add coordinate
                geoWKT += std::to_string(c.p) + " " + std::to_string(c.q) + ", ";
            }
            geoWKT.erase(geoWKT.length()-2);
            geoWKT += "), ";

        } else if (g->isType(GeometryType::Polygon)) {

            // Add polygon
            geoWKT += "POLYGON ((";
            auto coords = v.getPolygonCoordinates(index);
            auto p = std::static_pointer_cast<Polygon<T>>(g);
            auto subIndexes = p->getSubIndexes();
            cl_uint currentIndex = 0;
            cl_uint firstIndex = 0;
            for (uint32_t i = 0; i < coords.size(); i++) {

                // Add coordinate
                auto &c = coords[i];
                geoWKT += std::to_string(c.p) + " " + std::to_string(c.q) + ", ";

                // Create new entry for each sub-polygon
                if (--subIndexes[currentIndex] == 0) {

                    geoWKT.erase(geoWKT.length()-2);
                    geoWKT += "), (";

                    // Update indexes
                    currentIndex++;
                    firstIndex = i+1;
                }
            }
            geoWKT.erase(geoWKT.length()-3);
            geoWKT += "), ";
        }

        if (geoWKT.size() > 2) {
            geoWKT.erase(geoWKT.length()-2);
        }

        return geoWKT;
    }

    // Float type definitions
    template std::vector<cl_uint> GeoWKT<float>::parseString(Geostack::Vector<float> &v, std::string);
    template std::vector<cl_uint> GeoWKT<float>::parseStrings(Geostack::Vector<float> &v, std::vector<std::string>);
    template Geostack::Vector<float> GeoWKT<float>::geoWKTFileToVector(std::string);
    template Geostack::Vector<float> GeoWKT<float>::geoWKTToVector(std::string);
    template std::string GeoWKT<float>::vectorToGeoWKT(Geostack::Vector<float> &);
    template std::string GeoWKT<float>::vectorItemToGeoWKT(Geostack::Vector<float> &, std::size_t index);

    // Double type definitions
    template std::vector<cl_uint> GeoWKT<double>::parseString(Geostack::Vector<double> &v, std::string);
    template std::vector<cl_uint> GeoWKT<double>::parseStrings(Geostack::Vector<double> &v, std::vector<std::string>);
    template Geostack::Vector<double> GeoWKT<double>::geoWKTFileToVector(std::string);
    template Geostack::Vector<double> GeoWKT<double>::geoWKTToVector(std::string);
    template std::string GeoWKT<double>::vectorToGeoWKT(Geostack::Vector<double> &);
    template std::string GeoWKT<double>::vectorItemToGeoWKT(Geostack::Vector<double> &, std::size_t index);
}
