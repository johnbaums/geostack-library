/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
// Aligned access
#define _val2D_a(R, i, j) (*((R)+(i)+(j)*_dim.mx))
#define _val3D_a(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim.my)*_dim.mx))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

/**
* Map distance from point to raster.
* @param _u %Tile data.
* @param _dim %Dimensions of _u.
* @param _c Array of coordinates.
* @param _r Point width array.
* @param _v Array of z index levels.
* @param _n Number of coordinates.
*/
__kernel void map2DPointDistance(
    __global REAL *_u,
    const Dimensions _dim,
    __global REALVEC4 *_c, 
    __global REAL *_r,
    __global uint *_v,
    const uint _n/*__ARGS__*/
) {

    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i >= _dim.nx || _j >= _dim.ny) {
        _val3D_a(_u, _i, _j, _k) = NAN;
        return;
    }
    
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)_i+0.5)*_dim.hx+_dim.ox, 
        ((REAL)_j+0.5)*_dim.hy+_dim.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, &_vproj, &_rproj);

__PROJECT__*/
        
    // Read initial value
    REAL output = _val3D_a(_u, _i, _j, _k);
            
    // Loop over points
    for (uint index = 0; index < _n; index++) {

        // Skip levels
        if (*(_v+index) != _k) {
            continue;
        }

        // Get radius
        const REAL radius = _r[index];

        // Calculate displacement vector
        REALVEC2 d = p-_c[index].xy;

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

        // TODO distance is a very rough approximation for GCS, use Haversine great circle formula
    }

    // Write to raster
    _val3D_a(_u, _i, _j, _k) = output;
}

/**
* Map distance from line strings to raster.
* @param _u %Tile data.
* @param _dim %Dimensions of _u.
* @param _c Array of coordinates.
* @param _r Line width array.
* @param _o Array of offsets denoting the start of each line string.
* @param _v Array of z index levels.
* @param _n Length of offset array.
*/
__kernel void map2DLineStringDistance(
    __global REAL *_u,
    const Dimensions _dim,
    __global REALVEC4 *_c, 
    __global REAL *_r,
    __global uint *_o,
    __global uint *_v,
    const uint _n/*__ARGS__*/
) {
       
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i >= _dim.nx || _j >= _dim.ny) {
        _val3D_a(_u, _i, _j, _k) = NAN;
        return;
    }
    
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)_i+0.5)*_dim.hx+_dim.ox, 
        ((REAL)_j+0.5)*_dim.hy+_dim.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, &_vproj, &_rproj);

__PROJECT__*/

    // Read initial value
    REAL umin = _val3D_a(_u, _i, _j, _k);
        
    // Loop over lines
    uint lastoff = 0; 
    for (uint line = 0; line < _n; line++) {

        // Skip levels
        if (*(_v+line) != _k) {
            continue;
        }

        for (uint _idx = lastoff; _idx < _o[line]; _idx++) {

            // Get segment
            REALVEC2 l = _c[_idx+1].xy-_c[_idx].xy;
            REALVEC2 v = p-_c[_idx].xy;

            // Calculate Gram-Schmidt projection
            REAL dlv = dot(l, v);
            REAL dll = dot(l, l);
            if (dlv < 0.0 || dlv > dll) {

                // Projection onto line is outside line segment, use end points
                umin = fmin(umin, length(v)-_r[line]);
                v = p-_c[_idx+1].xy;
                umin = fmin(umin, length(v)-_r[line]);

                // TODO distance is a very rough approximation for GCS, user Haversine great circle formula

            } else {

                // Projection onto line is inside line segment, use distance to line
                REALVEC2 d = v-(dlv/dll)*l;
                umin = fmin(umin, length(d)-_r[line]);

                // TODO distance is a very rough approximation for GCS, user Haversine great circle formula
            }
        }

        // Update last offset
        lastoff = _o[line]+1;
    }

    // Write to raster
    _val3D_a(_u, _i, _j, _k) = umin;
}

/**
* Map distance from polygons to raster.
* @param _u %Tile data.
* @param _dim %Dimensions of _u.
* @param _c Array of coordinates.
* @param _o Array of offsets denoting the start of each polygon.
* @param _b Array of polygon bounding boxes as coordinates.
* @param _v Array of z index levels.
* @param _n Length of offset array.
*/
__kernel void map2DPolygonDistance(
    __global REAL *_u,
    const Dimensions _dim,
    __global REALVEC4 *_c,
    __global uint *_o,
    __global REALVEC4 *_b,
    __global uint *_v,
    const uint _n/*__ARGS__*/
) {
      
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i >= _dim.nx || _j >= _dim.ny) {
        _val3D_a(_u, _i, _j, _k) = NAN;
        return;
    }
        
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)_i+0.5)*_dim.hx+_dim.ox, 
        ((REAL)_j+0.5)*_dim.hy+_dim.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, &_vproj, &_rproj);

__PROJECT__*/

    // Read initial value
    REAL umin = _val3D_a(_u, _i, _j, _k);
        
    // Replace nodata with large value
    REAL sign = 1.0;
    if (umin != umin) {
        umin = MAXFLOAT;
    } else {
        sign = umin < 0.0 ? -1.0 : 1.0;
        umin = umin*umin;
    }
        
    // Loop over polygons
    uint lastoff = 0;  
    for (uint poly = 0; poly < _n; poly++) {

        // Skip levels
        if (*(_v+poly) != _k) {
            continue;
        }
        
        // Process if point is within polygon bounding box, 
        // or minimum distance to bounding box is less than current distance
        REALVEC2 c = p-clamp(p, _b[poly*2].lo, _b[poly*2+1].lo);
        if (dot(c, c) < umin) {

            // Map polygon
            int winding = 0;
            for (uint _idx = lastoff; _idx < _o[poly]; _idx++) {

                // Get segment
                REALVEC2 l = _c[_idx+1].xy-_c[_idx].xy;
                REALVEC2 v = p-_c[_idx].xy;

                // Check winding (http://geomalgorithms.com/a03-_inclusion.html)
                if (_c[_idx].y <= p.y) {
                    if (_c[_idx+1].y > p.y && (l.x*v.y-v.x*l.y) > 0.0)
                        winding++;
                } else {
                    if (_c[_idx+1].y <= p.y && (l.x*v.y-v.x*l.y) < 0.0)
                        winding--;
                }

                // Calculate Gram-Schmidt projection
                REAL dlv = dot(l, v);
                REAL dll = dot(l, l);
                if (dlv < 0.0 || dlv > dll) {

                    // Projection onto line is outside line segment, use end points
                    umin = fmin(umin, dot(v, v));
                    v = p-_c[_idx+1].xy;
                    umin = fmin(umin, dot(v, v));

                    // TODO distance is a very rough approximation for GCS, use Haversine great circle formula

                } else {

                    // Projection onto line is inside line segment, use distance to line
                    REALVEC2 d = v-(dlv/dll)*l;
                    umin = fmin(umin, dot(d, d));

                    // TODO distance is a very rough approximation for GCS, use Haversine great circle formula
                }
            }

            // If the point is inside any polygon, flip the sign
            if (winding != 0)
                sign = -sign;
        }

        // Update last offset
        lastoff = _o[poly]+1;
    }

    // Write to raster
    _val3D_a(_u, _i, _j, _k) = sign*sqrt(umin);
}

