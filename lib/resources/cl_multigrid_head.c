/**
* Multigrid redefinitions
*/
#undef _val2D_a
#undef _val2D_a_N
#undef _val2D_a_E
#undef _val2D_a_S
#undef _val2D_a_W
#undef _val2D_a_NE
#undef _val2D_a_SE
#undef _val2D_a_SW
#undef _val2D_a_NW

#define _val2D_a(R, i, j) (*((R)+(i)+((j)<</*__ROW_BSHIFT__*/)))

#define _val2D_a_N(R, i, j) (_boundary_N ? _val2D_a(R##_N, i, 0) : _val2D_a(R, i, j+1))
#define _val2D_a_E(R, i, j) (_boundary_E ? _val2D_a(R##_E, 0, j) : _val2D_a(R, i+1, j))
#define _val2D_a_S(R, i, j) (_boundary_S ? _val2D_a(R##_S, i, /*__MSIZE__*/-1) : _val2D_a(R, i, j-1))
#define _val2D_a_W(R, i, j) (_boundary_W ? _val2D_a(R##_W, /*__MSIZE__*/-1, j) : _val2D_a(R, i-1, j))

#define _val2D_a_NE(R, i, j) ((_boundary_N && _boundary_E) ? _val2D_a(R##_NE, 0, 0) : \
    (_boundary_E ? _val2D_a(R##_E,  0, j+1) : (_boundary_N ? _val2D_a(R##_N, i+1, 0) : _val2D_a(R, i+1, j+1))))
#define _val2D_a_SE(R, i, j) ((_boundary_S && _boundary_E) ? _val2D_a(R##_SE, 0, /*__MSIZE__*/-1) : \
    (_boundary_E ? _val2D_a(R##_E,  0, j-1) : (_boundary_S ? _val2D_a(R##_S, i+1, /*__MSIZE__*/-1) : _val2D_a(R, i+1, j-1))))
#define _val2D_a_SW(R, i, j) ((_boundary_S && _boundary_W) ? _val2D_a(R##_SW, /*__MSIZE__*/-1, /*__MSIZE__*/-1) : \
    (_boundary_W ? _val2D_a(R##_W, /*__MSIZE__*/-1, j-1) : (_boundary_S ? _val2D_a(R##_S, i-1, /*__MSIZE__*/-1) : _val2D_a(R, i-1, j-1))))
#define _val2D_a_NW(R, i, j) ((_boundary_N && _boundary_W) ? _val2D_a(R##_NW, /*__MSIZE__*/-1, 0) : \
    (_boundary_W ? _val2D_a(R##_W, /*__MSIZE__*/-1, j+1) : (_boundary_N ? _val2D_a(R##_N, i-1, 0) : _val2D_a(R, i-1, j+1))))

