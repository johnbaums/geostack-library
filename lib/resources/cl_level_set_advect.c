/**
* Level set step advect
*/

__kernel void advect(
const parameters _p,
uint _c,
__global uint *_ric,
__global RasterIndex *_ri,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24; 

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------   

        // Calculate derivative of level set for normed gradient
        REAL _dx = fmax(fmax(_distance-_distance_W, _distance-_distance_E)/_dim.hx, (REAL)0.0);
        REAL _dy = fmax(fmax(_distance-_distance_S, _distance-_distance_N)/_dim.hy, (REAL)0.0);
        REAL _mag = hypot(_dx, _dy);

        // Set default rate outside narrow band with speed 50% of maximum speed
        _rate = -0.5*_p.max_speed*_mag;

        // Time integrate with Euler step
        _distance_update = _distance+_p.dt*_rate;

        // Store speed
        _speed = 0.0;  
        
        // Store narrow band indexes
        if (fabs(_distance) < _p.band_width && _mag > 0.0) {

            RasterIndex ri;
            ri.i = (ushort)_i;
            ri.j = (ushort)_j;
            ri.k = (ushort)_k;
            ri.mag = _mag;
            atomic_inc(_ric+_c+1);
            *(_ri+atomic_inc(_ric)) = ri;
        }   

/*__POST__*/

    }
}


__kernel void advectInactive(
const parameters _p,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24; 

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------  

/*__POST__*/

    }
}

