/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
* General vector operator
*/
__kernel void vector(
    const uint _gn,
    __global uint *_gid/*__ARGS__*/
) {

    // Get geometry index
    const uint _gindex = get_global_id(0);

    // Check limits
    if (_gindex < _gn) {

        // Get property index
        const size_t _index = *(_gid+_gindex);

        // Keep flag
        bool keep = true;
        
/*__VARS__*/

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------
        
        // Apply filter
        if (keep) {
/*__POST__*/
        } else {
            *(_gid+_gindex) = noData_UINT;
        }
    }
}

