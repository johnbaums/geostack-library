/**
* Raster column sort
* Sort column values in z-direction
*/
__kernel void sortREAL(
    __global REAL *_u,
    const Dimensions _dim) {
        
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);    
    int n = _dim.nz;
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && n > 1) {

        // Patch nodata to infinity for sort
        for (int k = 0; k < n; k++) { 
            if (isInvalid_REAL(_val3D_a(_u, _i, _j, k))) {
                _val3D_a(_u, _i, _j, k) = INFINITY;
            }
        }
        
        // Build heap
        for (int k = 1; k < n; k++) { 
            if (_val3D_a(_u, _i, _j, k) > _val3D_a(_u, _i, _j, (k-1)/2)) { 
                int kk = k;
                while (_val3D_a(_u, _i, _j, kk) > _val3D_a(_u, _i, _j, (kk-1)/2)) { 
                    REAL tmp = _val3D_a(_u, _i, _j, kk);
                    _val3D_a(_u, _i, _j, kk) = _val3D_a(_u, _i, _j, (kk-1)/2);
                    kk = (kk-1)/2; 
                    _val3D_a(_u, _i, _j, kk) = tmp;
                }
            }
        }

        // Sort column
        int k = n-1;
        for (int k = n-1; k > 0; k--) { 
        
            REAL tmp = _val3D_a(_u, _i, _j, 0);
            _val3D_a(_u, _i, _j, 0) = _val3D_a(_u, _i, _j, k);
            _val3D_a(_u, _i, _j, k) = tmp;
          
            int kk = 0;
            int index = 0;
            do { 
                index = (kk*2)+1; 
                
                if (index < (k-1) && _val3D_a(_u, _i, _j, index) < _val3D_a(_u, _i, _j, index+1)) { 
                    index++;
                }
              
                if (index < k && _val3D_a(_u, _i, _j, kk) < _val3D_a(_u, _i, _j, index)) {
                    tmp = _val3D_a(_u, _i, _j, kk);
                    _val3D_a(_u, _i, _j, kk) = _val3D_a(_u, _i, _j, index);
                    _val3D_a(_u, _i, _j, index) = tmp;
                }
              
                kk = index; 
              
            } while(index < k);
        } 

        // Patch infinity to nodata
        for (int k = 0; k < n; k++) { 
            if (isinf(_val3D_a(_u, _i, _j, k))) {
                _val3D_a(_u, _i, _j, k) = noData_REAL;
            }
        }
    }
} 

