/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

// Runge-Kutta coefficients for Bogacki-Shampine
#define _C2 (REAL)0.5
#define _C3 (REAL)0.75
#define _C4 (REAL)1.0
#define _A21 (REAL)0.5
#define _A31 (REAL)0.0
#define _A32 (REAL)0.75
#define _A41 (REAL)0.22222222
#define _A42 (REAL)0.33333333
#define _A43 (REAL)0.44444444
#define _B1 (REAL)0.29166667
#define _B2 (REAL)0.25
#define _B3 (REAL)0.33333333
#define _B4 (REAL)0.125

/**
* Particle acceleration function
* @param position particle position vector
* @param velocity particle velocity vector
* @param advect particle advection vector
* @param time particle time
* @param radius particle radius
* @param _prsl particle random state vector
* @return instantaneous acceleration position
*/
inline REALVEC3 acceleration_fn(REALVEC3 position, REALVEC3 velocity, REALVEC3 advect, REAL time, REAL radius) {
    REALVEC3 acceleration = (REALVEC3)((REAL)0.0, (REAL)0.0, (REAL)0.0);

    // ---------------------------------
    // User defined code
/*__CODE1__*/
    // ---------------------------------

    return acceleration;
}

/**
* Particle update operation
* @param _cp particle position vector
* @param _vp particle position vector
* @param _n number of particles
* @param dt timestep
* @param _ci plane intersection count scalar
* @param _pi plane intersection index vector
* @param _pcx plane point x-component
* @param _pcy plane point y-component
* @param _pcz plane point z-component
* @param _pnx plane normal x-component
* @param _pny plane normal y-component
* @param _pnz plane normal z-component
*/
__kernel void update(
    __global Coordinate *_cp,
    __global Coordinate *_vp,
    const uint _n,
    const REAL dt,
    __global RandomState *_rs,
    __global uint *_pi,
    __global uint *_ci,
    const REAL _pcx,
    const REAL _pcy,
    const REAL _pcz,
    const REAL _pnx,
    const REAL _pny,
    const REAL _pnz/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);

    // Check limits
    if (index < _n) {

        // Get random state
        RandomState _rsl = *(_rs+index); // Random state
        RandomState *_prsl = &_rsl;      // Random state pointer
    
        // Get variables
        Coordinate _c = *(_cp+index);
        Coordinate _v = *(_vp+index);

        REAL time = _c.s;
        REAL radius = _v.s;  
        REALVEC3 position = (REALVEC3)(_c.p, _c.q, _c.r);
        REALVEC3 velocity = (REALVEC3)(_v.p, _v.q, _v.r);    
        REALVEC3 advect = (REALVEC3)(0.0, 0.0, 0.0);  
        
/*__VARS__*/

        // ---------------------------------
        // User defined code
/*__INIT1__*/
        // ---------------------------------

        if (radius >= 0.0) {

            // Adaptive Runge-Kutta (assumes first-same-as-last and B is the same as the last row of A)
            int _nsteps = 0;
            REAL _adt = dt;          
            REAL _error = 0.0;
            REALVEC3 _position_initial, _position_update;
            REALVEC3 _velocity_initial, _velocity_update;
            REALVEC3 _acceleration_initial;
            REALVEC3 _k1, _k2, _k3, _k4;
            REALVEC3 _l1, _l2, _l3, _l4;

            _position_initial = position;
            _velocity_initial = velocity;
            _acceleration_initial = acceleration_fn(position, velocity, advect, time, radius);

            do {

                // Set initial variables
                _k4 = _acceleration_initial;
                _l4 = _velocity_initial;
                position = _position_initial;
                velocity = _velocity_initial;

                for (int c = 0; c < (1<<_nsteps); c++) {

                    _k1 = _k4;
                    _l1 = _l4;                
                    _velocity_update = velocity+_adt*_A21*_k1;
                    _position_update = position+_adt*_A21*_l1;

                    _k2 = acceleration_fn(_position_update, _velocity_update, advect, time+_C2*_adt, radius);
                    _l2 = _velocity_update;                    
                    _velocity_update = velocity+_adt*(_A31*_k1+_A32*_k2);
                    _position_update = position+_adt*(_A31*_l1+_A32*_l2);

                    _k3 = acceleration_fn(_position_update, _velocity_update, advect, time+_C3*_adt, radius);
                    _l3 = _velocity_update;                    
                    _velocity_update = velocity+_adt*(_A41*_k1+_A42*_k2+_A43*_k3);
                    _position_update = position+_adt*(_A41*_l1+_A42*_l2+_A43*_l3);

                    _k4 = acceleration_fn(_position_update, _velocity_update, advect, time+_C4*_adt, radius);
                    _l4 = _velocity_update;

                    // Calculate error
                    _error = length(_velocity_update-(velocity+_adt*(_B1*_k1+_B2*_k2+_B3*_k3+_B4*_k4)));
                    if (_error > 1.0E-3)
                        break;

                    position = _position_update;
                    velocity = _velocity_update;
                }

                // Update timestep
                _adt*=0.5;
                _nsteps++;

            } while(_error > __TOLERANCE__ && _nsteps < __MAX_STEPS__);
            
/*__SAMPLE__
            // Calculate point-plane intersection
            REALVEC3 _pn = (REALVEC3)(_pnx, _pny, _pnz);
            REALVEC3 _pc = (REALVEC3)(_pcx, _pcy, _pcz);
            REAL _q = dot(_pc-_position_initial, _pn);
            if (_q == 0.0) {

                // Line is parallel or point is on plane
                uint _idx = atomic_inc(_ci);
                _pi[_idx] = index;

            } else {

                // Check for intercept
                REAL _qd = dot(position-_position_initial, _pn);
                if (_qd != 0.0) {
                    _q /= _qd;
                    if (_q >= 0.0 && _q <=1.0) {
                        uint _idx = atomic_inc(_ci);
                        _pi[_idx] = index;
                    }
                }
            }
__SAMPLE__*/

            // ---------------------------------
            // User defined code
/*__POST1__*/
            // ---------------------------------

        }

/*__POST2__*/

        // Store particle parameters
        _c.p = position.x;
        _c.q = position.y;
        _c.r = position.z;
        _c.s = time;
        _v.p = velocity.x;
        _v.q = velocity.y;
        _v.r = velocity.z;
        _v.s = radius;

        *(_cp+index) = _c;
        *(_vp+index) = _v;

        // Store state
        *(_rs+index) = _rsl;
    }
}

inline REALVEC3 mu_fn(REALVEC3 position, REALVEC3 advect) {
    REALVEC3 mu = (REALVEC3)((REAL)0.0, (REAL)0.0, (REAL)0.0);

    // ---------------------------------
    // User defined code
/*__CODE2__*/
    // ---------------------------------

    return mu;
}

inline REALVEC3 sigma_fn(REALVEC3 position, REALVEC3 advect) {
    REALVEC3 sigma = (REALVEC3)((REAL)0.0, (REAL)0.0, (REAL)0.0);

    // ---------------------------------
    // User defined code
/*__CODE3__*/
    // ---------------------------------

    return sigma;
}

/**
* Particle stochastic update operation
*/
__kernel void updateStochastic(
    __global Coordinate *_cp,
    const uint _n,
    const REAL dt,
    __global RandomState *_rs/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);

    // Check limits
    if (index < _n) {
    
        // Get variables
        Coordinate _c = *(_cp+index);

        // Get random state
        RandomState _rsl = *(_rs+index); // Random state
        RandomState *_prsl = &_rsl;        // Random state pointer

        REAL time = _c.s;
        REALVEC3 position = (REALVEC3)(_c.p, _c.q, _c.r);  
        REALVEC3 advect = (REALVEC3)(0.0, 0.0, 0.0);  
        
/*__VARS__*/

    // ---------------------------------
    // User defined code
/*__INIT2__*/
    // ---------------------------------

        // Stochastic Euler
        REALVEC3 mu = mu_fn(position, advect);
        REALVEC3 sigma = sigma_fn(position, advect);
        REALVEC3 dW = (REALVEC3)(0.0, 0.0, 0.0); 

    // ---------------------------------
    // User defined code
/*__CODE4__*/
    // ---------------------------------

        position += mu*dt+sigma*dW;
        
/*__POST2__*/

        // Store particle parameters
        _c.p = position.x;
        _c.q = position.y;
        _c.r = position.z;
        _c.s = time;

        *(_cp+index) = _c;

        // Store state
        *(_rs+index) = _rsl;
    }
}

